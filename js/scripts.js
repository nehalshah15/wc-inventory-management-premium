jQuery(function ($) {

    var supplier_variation = {
        init: function () {
            $('#variable_product_options').on('change', 'input.variable_is_supplier', this.variable_is_supplier);
            $('#frm_product_handler').submit(this.add_product_manually);
            $('#btnCreateProductFile').click(this.create_product_file);
            $('.btnOrderFullyArrived').click(this.complete_request_product_order);
            $('.btnOrderSave').click(this.complete_request_product_order);
            $('.add_unit_btn').click(this.add_input_units);
            $('#setting_table').on('click', 'a.unit_save', this.unit_save);
            $('#setting_table').on('click', 'a.unit_delete', this.unit_delete);
            $(".unit_save").hide();
            if($("#display_units tr").length > 2){
                $(".unit_save").show();
            }
            $("#update_database").click(this.get_total_products_to_update);
        },
        variable_is_supplier: function () {
            $(this).closest('.woocommerce_variation').find('.show_if_variation_manage_supplier').hide();

            if ($(this).is(':checked')) {
                $(this).closest('.woocommerce_variation').find('.show_if_variation_manage_supplier').show();
            }
        },
        add_product_manually: function () {
            $('#frm_product_handler input').removeClass('error');
            if (!$('#frm_product_handler input[name="product_sku"]').val().length) {
                $('#frm_product_handler input[name="product_sku"]').addClass('error');
            }
            if (!$('#frm_product_handler input[name="qty"]').val().length) {
                $('#frm_product_handler input[name="qty"]').addClass('error');
            }
            if ($('#frm_product_handler input').hasClass('error')) {
                return false;
            }
            $.ajax({
                url: ajaxurl,
                data: $('#frm_product_handler').serialize(),
                method: 'POST',
                success: function (data) {
                    datavalues = JSON.parse(data);
                    if (datavalues['success']) {
                        if ($('.wp-list-table input[data-id="' + datavalues['id'] + '"]').length) {
                            alert(datavalues['message']);
                        } else {
                            $('#frm_product_handler').trigger('reset');
                            $('.wp-list-table tbody').append(datavalues['data']);
                        }
                    } else {
                        alert(datavalues['message']);
                    }
                },
                error: function (data) {
                    alert(data);
                }
            });
            return false;
        },
        create_product_file: function () {
            $('.inventory_management_panel.blockUI').addClass('blockOverlay');
            $.ajax({
                url: ajaxurl,
                data: $('.product_listing_panel form').serialize(),
                method: 'POST',
                success: function (data) {
                    datavalues = JSON.parse(data);
                    //$('.inventory_management_panel.blockUI').removeClass('blockOverlay');
                    if (datavalues['success']) {
                        if (datavalues['product_id'] && datavalues['product_id'].length) {
                            for ($i = 0; $i < datavalues['product_id'].length; $i++) {
                                //$('.wp-list-table input[data-id="'+ datavalues['product_id'][$i] +'"]').parents('tr').remove();
                            }
                            if ($('.wp-list-table tbody tr').length == 0) {
                                $('.wp-list-table tbody').append(datavalues['default']);
                            }
                        }
                        //$('.order_product_files').replaceWith( datavalues['orders'] );
                        window.location.reload();
                        //alert( datavalues['message'] );
                    } else {
                        alert(datavalues['message']);
                        $('.inventory_management_panel.blockUI').removeClass('blockOverlay');
                    }
                },
                error: function (data) {
                    $('.inventory_management_panel.blockUI').removeClass('blockOverlay');
                    alert(data);
                }
            });
            return false;
        },
        complete_request_product_order: function () {
            $obj = $(this);
            $row_id = $(this).data('id');
            $('input[type="text"]').removeClass('error');
            if ($(this).data('product_id')) {
                $data = 'action=complete_product_order&id=' + $row_id + '&product_id=' + $(this).data('product_id');
            } else {
                if ($('input[data-id="' + $row_id + '"]').val() == '') {
                    $('input[type="text"][data-id="' + $row_id + '"]').addClass('error');
                    return false;
                }
                $data = 'action=complete_product_order&id=' + $row_id + '&qty=' + $('input[data-id="' + $row_id + '"]').val();
            }
            $($obj).parent('.action').addClass('blockUI').addClass('blockOverlay');
            $.ajax({
                url: ajaxurl,
                data: $data,
                method: 'POST',
                success: function (data) {
                    datavalues = JSON.parse(data);
                    if (datavalues['success']) {
                        $('.wp-list-table .btnOrderFullyArrived[data-id="' + datavalues['row_id'] + '"]').parents('tr').remove();
                        if ($('.wp-list-table tbody tr').length == 0) {
                            $('.wp-list-table tbody').append(datavalues['default']);
                        }
                        $('.order_product_files').replaceWith(datavalues['orders']);
                        var dname = $('select[name="supplier_id"] option:selected').data('name');
                        var dno = $('select[name="supplier_id"] option:selected').attr('data-no');
                        dno = dno - 1;
                        $('select[name="supplier_id"] option:selected').attr('data-no', dno);
                        $('select[name="supplier_id"] option:selected').html(dname + ' (' + dno + ')');
                        //alert( datavalues['message'] );
                    } else {
                        $($obj).parent('.action').removeClass('blockOverlay');
                        alert(datavalues['message']);
                    }
                },
                error: function (data) {
                    $($obj).parent('.action').removeClass('blockOverlay');
                    alert(data);
                }
            });
            return false;
        },
        add_input_units: function () {
            var html = '<tr><td><input type="text" name="singular_unit[]" class="singular_unit" placeholder="'+wooic_obj.singular_ex+'">\n\
                        </td><td><input type="text" name="plural_unit[]" class="plural_unit" placeholder="'+wooic_obj.plural_ex+'">\n\
                        </td><td><a href="javascript:void(0);" class="unit_delete button button-primary button-large">'+wooic_obj.cancel_text+'</a>\n\
                        </td></tr>';
            $("table#display_units").append(html);
            $(".unit_save").show();
        },
        unit_save: function () {
            var selected_option = $('#im_units').val();
            var singular_unit = $("input[name^=singular_unit]").map(function(){
                if($(this).val() != ""){
                    return $(this).val();
                }
            }).get();
            var plural_unit = $("input[name^=plural_unit]").map(function(){
                if($(this).val() != ""){
                    return $(this).val();
                }
            }).get();
            if(singular_unit && plural_unit ) {
                $.ajax({
                    url: ajaxurl,
                    data: {action:'save_unit_to_meta',singular:singular_unit,plural:plural_unit},
                    method: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if(data){
                            $("#im_units").find('option').remove();
                            $.each(data.units, function(key, value) {   
                                $("#im_units").append($("<option></option>").attr("value",key) .text(key+"/"+value)); 
                            });                             
                            alert(data.msg);
                            $("select option[value='"+selected_option+"']").attr("selected","selected");
                            $(".unit_delete").text(wooic_obj.delete_text);
                        }
                    }
                });
            }
        },
        unit_delete: function () {
            var current_tr = $(this).closest('tr');
            var unit_key = $(this).closest('tr').find(".singular_unit").val();
            if(unit_key){
                if (confirm(wooic_obj.confirm_msg))
                {
                    $.ajax({
                        url: ajaxurl,
                        data: {action:'delete_unit_from_meta',unit_key:unit_key},
                        method: 'POST',
                        dataType: 'json',
                        success: function (data) {
                            if(data){
                                $(current_tr).remove();
                                $('#im_units option').each(function(){
                                    if (this.value == unit_key) {
                                        $("#im_units option[value='"+unit_key+"']").remove();
                                    }
                                });
                                if($("#display_units tr").length == 2){
                                    $(".unit_save").hide();
                                }
                            }
                        }
                    });
                }
            }else{
                $(current_tr).remove();
                if($("#display_units tr").length == 2){
                    $(".unit_save").hide();
                }
            }
        },
        get_total_products_to_update:function(){
            $.ajax({
                url: ajaxurl,
                data: {action:'get_product_data_to_update'},
                method: 'POST',
                success: function (response) {
                    if(response){
                        var data = JSON.parse(response);   
                        total_found_post = data.found_post;
                        if(total_found_post > 0){
                            $("#myProgress").show();
                            supplier_variation.add_default_data_to_existing_products();  
                        }
                    }
                }
            });
        },
        add_default_data_to_existing_products:function(){
            $.ajax({
                url: ajaxurl,
                data: {action:'add_default_data_to_existing_product'},
                method: 'POST',
                success: function (response) {
                    if(response){
                        var data = JSON.parse(response);
                        var elem = $("#myBar");
                        if(data.found_post != 0){
                            var percentage = 100 - Math.floor((data.found_post*100)/total_found_post);
                            elem.css("width",percentage+"%");
                            elem.html(percentage+"%");
                            supplier_variation.add_default_data_to_existing_products(); 
                        }else{
                            elem.css("width","100%");
                            elem.html("100%");
                            setTimeout(function(){ 
                                $("#myProgress").hide();
                                window.location.reload();
                            }, 1000);
                            
                        }
                    }
                }
            });
        }
    };
    supplier_variation.init();
});