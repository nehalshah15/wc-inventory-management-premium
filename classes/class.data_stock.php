<?php
// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class DataStock extends WP_List_Table
{
    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        usort( $data, array( &$this, 'sort_data' ) );

        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'date'       		=>  __('Date','wooic')
        );
		$currency = $this->get_used_currency();
		if( $currency ){
			foreach( $currency as $curr ){
				$columns['product_in_'. strtolower($curr['currency']) ] = $curr['currency'];
			}
		}
        return $columns;
    }
	
	public function get_used_currency(){
		global $wpdb;
		$args = array(
	'post_type' => array('supplier'),
				'post_status' => array( 'private', 'publish' ),
				'meta_key'=>'wcim_supplier_currency'
			);
		$supplier_list = new WP_Query( $args );
		$supplier_ids = array();
		if( $supplier_list->have_posts() ){
			while( $supplier_list->have_posts() ){
				$supplier_list->the_post();
				$supplier_ids[] = get_the_ID();
			}
		}
		$suppliers = implode(',', $supplier_ids);
		if(count($supplier_ids) > 0)
		{
			$sql =  "SELECT meta_value AS currency FROM {$wpdb->prefix}postmeta WHERE `meta_key` LIKE 'wcim_supplier_currency' and post_id in($suppliers) GROUP BY meta_value";
			$result = $wpdb->get_results( $sql , ARRAY_A);
			return $result;
		}
		return;
		
	}

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('product_name' => array('product_name', false));
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
		global $default_supplier, $wpdb;
        $data = array();

		wp_enqueue_style('woocommerce_admin_styles');
		
		$year = date("Y");
		if( isset( $_GET['year_stock'] ) && $_GET['year_stock'] != '' ){
			$year = $_GET['year_stock'];
		}
		
		$sql = "SELECT *  FROM `{$wpdb->prefix}postmeta` WHERE `meta_key` LIKE '%wcim_supplier_id%'";
		$product_list = $wpdb->get_results( $sql );
		$data = array();

		$currency = $this->get_used_currency();
		$default_our_pack_size = get_option('wcim_default_our_pack_size');
        $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
                
		if( $product_list ){
			foreach( $product_list as $product_row ){
				$id 				= $product_row->post_id;
				$supplier_id 		= $product_row->meta_value;
				$arrvived_stock		= get_post_meta( $id, '_stock', true );
				$date				= date('n');
				$product_status = get_post_status($id);
				$product 			= get_post( $id );	
				$purchase_price 	= get_post_meta( $id, 'wcim_supplier_purchase_price', true );
				$purchase_currency 	= get_post_meta( $supplier_id, 'wcim_supplier_currency', true );
				$purchase_price		= $purchase_price ? $purchase_price : 0;
				if( !$purchase_price && $product->post_parent ){
					$purchase_price 	= get_post_meta( $product->post_parent, 'wcim_supplier_purchase_price', true );
					$purchase_price		= $purchase_price ? $purchase_price : 0;
				}
				//$key_date = strtotime( $date );
				$key_date = $date;
				$data[ $key_date ]['date']	=	$date;
                                $our_pack_size      = get_post_meta( $id, 'wcim_our_pack_size', true );
                                $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
                                $supplier_remaining_pieces = get_post_meta($id, 'wcim_supplier_remaining_pieces', true);
                                if($supplier_remaining_pieces != '')
                                {
									$total_pieces = floor(($arrvived_stock*$our_pack)+$supplier_remaining_pieces);
								}
                                $supplier_pack_size = get_post_meta( $id, 'wcim_supplier_pack_size', true );
                                $supplier_pack = $supplier_pack_size ? $supplier_pack_size : ($default_supplier_pack_size ? $default_supplier_pack_size : 1);
                                $purchase_amount = $purchase_price/$supplier_pack;
                                if($product_status == "publish"){ 
                                    if( $currency ){
                                            foreach( $currency as $curr ){
                                                    if( $curr['currency'] ==  $purchase_currency ){
                                                            $curr = strtolower($curr['currency']);
                                                            $previous_amount = 0;
                                                            if( isset( $data[ $key_date ]['product_in_'. $curr ] ) ){
                                                                    $previous_amount = $data[ $key_date ]['product_in_'. $curr ];
                                                            }	
                                                            $data[ $key_date ]['product_in_'. $curr ] = $previous_amount + ( $purchase_amount * $total_pieces );
                                                    }else{
                                                            $curr = strtolower($curr['currency']);
                                                            if( !isset( $data[ $key_date ]['product_in_'. $curr ] ) ){
                                                                    $data[ $key_date ]['product_in_'. $curr ] = 0;	
                                                            }
                                                    }
                                            }
                                    }
                                }
			}
		}
		$stock_data = get_option( 'wcim_stock_data' );	
		if( $stock_data && is_array( $stock_data ) ){
			$stock_year = isset( $_GET['year_stock'] ) && $_GET['year_stock'] != '' ? $_GET['year_stock'] : date('Y');
			foreach( $stock_data[ $stock_year ] as $month => $stock ){
				if( $currency ){
					$key_date = $month;
					$data[ $key_date ]['date'] = $key_date;
					foreach( $currency as $curr ){
						if( array_key_exists( $curr['currency'], $stock ) ){
							$currencies = strtolower($curr['currency']);
							$data[ $key_date ]['product_in_'. $currencies ] = $stock[$curr['currency']];
						}
					}
				}       
			}
		}
		return $data;
		
		$sql = "SELECT product_id, supplier_id, sum( arrvived_stock ) AS arrvived_stock, DATE_FORMAT(order_date, '%m-%Y') AS stock_date FROM `{$wpdb->prefix}order_inventory` WHERE DATE_FORMAT(order_date, '%Y') = $year group by stock_date, product_id order by stock_date DESC";
		
		$product_list = $wpdb->get_results( $sql );
		$data = array();

		$currency = $this->get_used_currency();
		
		if( $product_list ){
			foreach( $product_list as $product_row ){
				$id 				= $product_row->product_id;
				$supplier_id 		= $product_row->supplier_id;
				$arrvived_stock		= $product_row->arrvived_stock;
				$date				= $product_row->stock_date;
				
				$product 			= get_post( $id );	
				$purchase_price 	= get_post_meta( $id, 'wcim_supplier_purchase_price', true );
				$purchase_currency 	= get_post_meta( $supplier_id, 'wcim_supplier_currency', true );
				$purchase_price		= $purchase_price ? $purchase_price : 0;
				if( !$purchase_price && $product->post_parent ){
					$purchase_price 	= get_post_meta( $product->post_parent, 'wcim_supplier_purchase_price', true );
					$purchase_price		= $purchase_price ? $purchase_price : 0;
				}
				//$key_date = strtotime( $date );
				$key_date = $date;
				$data[ $key_date ]['date']	=	$date;
				$our_pack_size      = get_post_meta( $id, 'wcim_our_pack_size', true );
                                $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
                                $supplier_remaining_pieces = get_post_meta($id, 'wcim_supplier_remaining_pieces', true);
                                $total_pieces = floor($arrvived_stock*$our_pack+$supplier_remaining_pieces);
				if( $currency ){
					foreach( $currency as $curr ){
						if( $curr['currency'] ==  $purchase_currency ){
							$curr = strtolower($curr['currency']);
							$previous_amount = 0;
							if( isset( $data[ $key_date ]['product_in_'. $curr ] ) ){
								$previous_amount = $data[ $key_date ]['product_in_'. $curr ];
							}
							$data[ $key_date ]['product_in_'. $curr ] = $previous_amount + ( $purchase_price * $total_pieces );
						}else{
							$curr = strtolower($curr['currency']);
							if( !isset( $data[ $key_date ]['product_in_'. $curr ] ) ){
								$data[ $key_date ]['product_in_'. $curr ] = 0;	
							}
						}
					}
				}
			}
		}              
		$data = array_values( $data );
        return $data;
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
		$currency = $this->get_used_currency();
		if( $currency ){
			foreach( $currency as $curr ){
				if( 'product_in_'. strtolower($curr['currency']) == $column_name ){
					if( isset( $item[ $column_name ] ) ){
						return number_format($item[ $column_name ],2).' '.get_woocommerce_currency_symbol( $curr['currency'] );	
					}else{
						return '0 '.get_woocommerce_currency_symbol( $curr['currency'] );	
					}
				}
			}
		}
        switch( $column_name ) {
            case 'date':
                $current_month = date('n');
                $last_date_of_month = date("Y")."-".$item[ $column_name ].'-'.'01';
                if($item[ $column_name ] == $current_month){
                    $last_date =  __("Current stock value",'wooic');
                }else{
                    $last_date = date("Y-m-t",strtotime($last_date_of_month));
                }
                return $last_date;
            default:
                return $item[ $column_name ];
        }
    }
	
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'date';
        $order = 'desc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }
	
}
	
?>
