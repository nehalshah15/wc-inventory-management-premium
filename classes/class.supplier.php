<?php

if (!class_exists('WooICP_Supplier')) {

    class WooICP_Supplier {

        private $product_id, $parent_id;

        function __construct() {

            add_filter('woocommerce_product_data_tabs', array($this, 'supplier_data_tabs'));
            add_action('woocommerce_product_data_panels', array($this, 'supplier_data_panel'));
            add_action('woocommerce_product_after_variable_attributes', array($this, 'supplier_info_baseon_varible'), 10, 3);
            add_action('save_post', array($this, 'save_product_supplier'));
            add_action('woocommerce_save_product_variation', array($this, 'save_supplier_variation'), 10, 2);
            add_filter('enter_title_here', array($this, 'enter_title_here'));
        }

        function enter_title_here($title) {
            $screen = get_current_screen();

            if ($screen->post_type == 'supplier') {
                $title = __('Enter full company name', 'wooic');
            }
            return $title;
        }

        function supplier_data_tabs($tabs) {
            wp_enqueue_style('wooicp_style');
            $tabs['supplier_tab'] = array(
                'label' => __('Inventory management', 'wooic'),
                'target' => 'suppliers_data_panel',
                'class' => array(),
                'priority' => 80);
            return $tabs;
        }

        function supplier_data_panel() {
            global $post, $woocommerce;
            $supplier_id = get_post_meta($post->ID, 'wcim_supplier_id', true);
            if (!$supplier_id) {
                $supplier_id = get_post_meta($post->post_parent, 'wcim_supplier_id', true);
            }
            $supplier_currency = get_post_meta($supplier_id, 'wcim_supplier_currency', true);
            $default_currency = get_option('wcim_default_supplier_currency');
            $our_pack_size = get_post_meta($post->ID, 'wcim_our_pack_size', true);
            $default_our_pack_size = get_option('wcim_default_our_pack_size');
            $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
            $supplier_pack_size = get_post_meta($post->ID, 'wcim_supplier_pack_size', true);
            $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
            $supplier_pack = $supplier_pack_size ? $supplier_pack_size : ($default_supplier_pack_size ? $default_supplier_pack_size : 1);
            $units = get_option('wcim_units');
            $selected_unit = get_post_meta($post->ID,'wcim_supplier_unit',true);
            $default_unit = get_option('wcim_default_unit');
            if($default_unit == '')
            {
				return;
			}
            $unit = $selected_unit ? $selected_unit : ($default_unit ? $default_unit : "piece");
            if (!array_key_exists($selected_unit,$units))
            {
                if(array_key_exists($default_unit,$units)){
                    $unit = $default_unit;
                }else{
                    $unit="piece";
                }
            }else{
                $unit = $selected_unit;
            }
            $supplier_show_in_low_stock = get_post_meta($post->ID,'wcim_supplier_show_in_low_stock',true);
            if($supplier_show_in_low_stock == "yes"){
                $supplier_show_in_low_stock = "yes";
            }else if($supplier_show_in_low_stock == "no"){
                $supplier_show_in_low_stock = "no";
            }else{
                $supplier_show_in_low_stock = "yes";
            }
            $current_stock = get_post_meta($post->ID, '_stock', true);
            $supplier_remaining_pieces = get_post_meta($post->ID, 'wcim_supplier_remaining_pieces', true);
            update_post_meta($post->ID, 'wcim_previous_stock', $current_stock);
            update_post_meta($post->ID, 'wcim_previous_pack_size', $our_pack);
            if($supplier_remaining_pieces != '')
            {
				$total_pieces = floor($current_stock*$our_pack+$supplier_remaining_pieces);
				update_post_meta($post->ID, 'wcim_previous_total_pieces',$total_pieces);
				$purchase_price =  get_post_meta($post->ID,'wcim_supplier_purchase_price',true);
				include_once( WOOICP_TEMPLATE . 'product_supplier.php' );
			}
            
        }

        function supplier_info_baseon_varible($loop, $variation_data, $variation) {
            global $post, $woocommerce;
            $this->product_id = $variation->ID;
            $this->parent = $variation->post_parent;
            $supplier_id = get_post_meta($variation->ID, 'wcim_supplier_id', true);
            if (!$supplier_id) {
                $supplier_id = get_post_meta($variation->post_parent, 'wcim_supplier_id', true);
            }
            $supplier_currency = get_post_meta($supplier_id, 'wcim_supplier_currency', true);
            $default_currency = get_option('wcim_default_supplier_currency');
            $default_our_pack_size = get_option('wcim_default_our_pack_size');
            $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
            $parent_supplier_pack_size = get_post_meta($variation->post_parent, 'wcim_supplier_pack_size', true);
            $parent_our_pack_size = get_post_meta($variation->post_parent, 'wcim_our_pack_size', true);
            $our_pack = $this->get_data('wcim_our_pack_size') ? $this->get_data('wcim_our_pack_size') : ($parent_our_pack_size ? $parent_our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1));
            $supplier_pack = $this->get_data('wcim_supplier_pack_size') ? $this->get_data('wcim_supplier_pack_size') : ($parent_supplier_pack_size ? $parent_supplier_pack_size : ($default_supplier_pack_size ? $default_supplier_pack_size : 1));
            $units = get_option('wcim_units');
            $parent_unit = get_post_meta($variation->post_parent,'wcim_supplier_unit',true);
            $default_unit = get_option('wcim_default_unit');
            $unit = $this->get_data('wcim_supplier_unit') ? $this->get_data('wcim_supplier_unit') : ($parent_unit ? $parent_unit : ($default_unit ? $default_unit : "piece"));
            if (!array_key_exists($this->get_data('wcim_supplier_unit'),$units))
            {
                if(!array_key_exists($parent_unit,$units)){
                    if(array_key_exists($default_unit,$units)){
                        $unit = $default_unit;
                    }else{
                        $unit="piece";  
                    }

                }else{
                    $unit = $parent_unit;
                }  
            }else{
                $unit = $this->get_data('wcim_supplier_unit');
            }
            $parent_show_in_low_stock = get_post_meta($variation->post_parent,'wcim_supplier_show_in_low_stock',true);
            if($this->get_data('wcim_supplier_show_in_low_stock')){
                if($this->get_data('wcim_supplier_show_in_low_stock') == "yes"){
                    $supplier_show_in_low_stock = "yes";
                }else if($this->get_data('wcim_supplier_show_in_low_stock') == "no"){
                    $supplier_show_in_low_stock = "no";
                }
            }else if($parent_show_in_low_stock){
                $supplier_show_in_low_stock = $parent_show_in_low_stock;
            }else{
                $supplier_show_in_low_stock = "yes";
            }
            $current_stock = get_post_meta($variation->ID, '_stock', true);
            $supplier_remaining_pieces = get_post_meta($variation->ID, 'wcim_supplier_remaining_pieces', true);
            update_post_meta($variation->ID, 'wcim_previous_stock', $current_stock);
            update_post_meta($variation->ID, 'wcim_previous_pack_size', $our_pack);
            $total_pieces = floor($current_stock*$our_pack+$supplier_remaining_pieces);
            update_post_meta($variation->ID, 'wcim_previous_total_pieces',$total_pieces);
            include( WOOICP_TEMPLATE . 'variable_supplier.php' );
        }

        function save_product_supplier($product_id) {
            if (wp_is_post_revision($product_id)) {
                return;
            }
            $post_type = get_post_type($product_id);

            // If this isn't a 'supplier' post, don't update it.
            if ("product" != $post_type) {
                return;
            }
            if (!isset($_POST['wcim_supplier_id'])) {
                return;
            }
            $previous_stock = get_post_meta($product_id, 'wcim_previous_stock', true);
            $previous_pack_size = get_post_meta($product_id, 'wcim_previous_pack_size', true);   
            $previous_total_pieces = get_post_meta($product_id, 'wcim_previous_total_pieces', true);
            $new_stock = $_POST['_stock'];
            $new_our_pack_size = $_POST['wcim_our_pack_size'];
            $new_total_pieces = $_POST['wcim_supplier_total_pieces'];
            update_post_meta($product_id, 'wcim_supplier_id', $_POST['wcim_supplier_id']);
            update_post_meta($product_id, 'wcim_supplier_art_id', $_POST['wcim_supplier_art_id']);
            update_post_meta($product_id, 'wcim_supplier_product_url', $_POST['wcim_supplier_product_url']);
            update_post_meta($product_id, 'wcim_supplier_warning_level', $_POST['wcim_supplier_warning_level']);
            update_post_meta($product_id, 'wcim_supplier_note', $_POST['wcim_supplier_note']);
            update_post_meta($product_id, 'wcim_supplier_purchase_price', $_POST['wcim_supplier_purchase_price']);
            update_post_meta($product_id, 'wcim_manual_pack_size_setting', $_POST['wcim_manual_pack_size_setting']);
            if($_POST['wcim_supplier_pack_size'] > 0){
                update_post_meta($product_id, 'wcim_supplier_pack_size', $_POST['wcim_supplier_pack_size']);
            }else{
                $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
                update_post_meta($product_id, 'wcim_supplier_pack_size', $default_supplier_pack_size);
            }
            if($_POST['wcim_our_pack_size'] > 0){
                update_post_meta($product_id, 'wcim_our_pack_size', $new_our_pack_size);
            }else{
                $default_our_pack_size = get_option('wcim_default_our_pack_size');
                update_post_meta($product_id, 'wcim_our_pack_size', $default_our_pack_size);
            }
            update_post_meta($product_id, 'wcim_supplier_unit', $_POST['wcim_supplier_unit']);
            if($_POST['wcim_supplier_show_in_low_stock']){
                update_post_meta($product_id, 'wcim_supplier_show_in_low_stock', "yes");
            }else{
                update_post_meta($product_id, 'wcim_supplier_show_in_low_stock', "no");
            }
            if(($previous_total_pieces != $new_total_pieces && $previous_stock != $new_stock) ||
                ($previous_total_pieces != $new_total_pieces && $previous_pack_size != $new_our_pack_size && $previous_stock != $new_stock)){
                $stock = floor($new_total_pieces/$new_our_pack_size);
                $remaining_pieces = fmod($new_total_pieces,$new_our_pack_size);
                update_post_meta($product_id, 'wcim_supplier_remaining_pieces', $remaining_pieces);
                update_post_meta($product_id, 'wcim_supplier_total_pieces', $new_total_pieces);
                update_post_meta($product_id, '_stock', $stock);
            }
            else if($previous_stock != $new_stock || 
               ($previous_stock != $new_stock && $previous_pack_size != $new_our_pack_size)){
                $total_pieces =  floor($new_stock*$new_our_pack_size);
                update_post_meta($product_id, 'wcim_supplier_remaining_pieces', 0);
                update_post_meta($product_id, 'wcim_supplier_total_pieces', $total_pieces);
                update_post_meta($product_id, '_stock', $new_stock);
            }else if($previous_total_pieces != $new_total_pieces || 
               $previous_pack_size != $new_our_pack_size || 
               ($previous_total_pieces != $new_total_pieces && $previous_pack_size != $new_our_pack_size)){
                $stock = floor($new_total_pieces/$new_our_pack_size);
                $remaining_pieces = fmod($new_total_pieces,$new_our_pack_size);
                update_post_meta($product_id, 'wcim_supplier_remaining_pieces', $remaining_pieces);
                update_post_meta($product_id, 'wcim_supplier_total_pieces', $new_total_pieces);
                update_post_meta($product_id, '_stock', $stock);
            }
            
        }

        function save_supplier_variation($variation_id, $i) {
            $previous_stock = get_post_meta($variation_id, 'wcim_previous_stock', true);   
            $previous_pack_size = get_post_meta($variation_id, 'wcim_previous_pack_size', true);   
            $previous_total_pieces = get_post_meta($variation_id, 'wcim_previous_total_pieces', true);
            $new_stock = $_POST['variable_stock'][$i];
            $new_our_pack_size = $_POST['wcim_our_pack_size'][$i];
            $new_total_pieces = $_POST['wcim_supplier_total_pieces'][$i];
            update_post_meta($variation_id, 'wcim_supplier_purchase_price', $_POST['wcim_supplier_purchase_price'][$i]);
            update_post_meta($variation_id, 'wcim_supplier_id', $_POST['wcim_supplier_id'][$i]);
            update_post_meta($variation_id, 'wcim_supplier_art_id', $_POST['wcim_supplier_art_id'][$i]);
            update_post_meta($variation_id, 'wcim_supplier_product_url', $_POST['wcim_supplier_product_url'][$i]);
            update_post_meta($variation_id, 'wcim_supplier_warning_level', $_POST['wcim_supplier_warning_level'][$i]);
            update_post_meta($variation_id, 'wcim_supplier_note', $_POST['wcim_supplier_note'][$i]);
            if($_POST['wcim_supplier_pack_size'][$i] > 0){
                update_post_meta($variation_id, 'wcim_supplier_pack_size', $_POST['wcim_supplier_pack_size'][$i]);
            }else{
                $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
                update_post_meta($variation_id, 'wcim_supplier_pack_size', $default_supplier_pack_size);
            }
            if($_POST['wcim_our_pack_size'][$i] > 0){
                update_post_meta($variation_id, 'wcim_our_pack_size', $new_our_pack_size);
            }else{
                $default_our_pack_size = get_option('wcim_default_our_pack_size');
                update_post_meta($variation_id, 'wcim_our_pack_size', $default_our_pack_size);
            }
            update_post_meta($variation_id, 'wcim_supplier_unit', $_POST['wcim_supplier_unit'][$i]);
            if($_POST['wcim_supplier_show_in_low_stock'][$i]){
                update_post_meta($variation_id, 'wcim_supplier_show_in_low_stock', "yes");
            }else{
                update_post_meta($variation_id, 'wcim_supplier_show_in_low_stock', "no");
            }
            if(($previous_total_pieces != $new_total_pieces && $previous_stock != $new_stock) ||
                ($previous_total_pieces != $new_total_pieces && $previous_pack_size != $new_our_pack_size && $previous_stock != $new_stock)){
                $stock = floor($new_total_pieces/$new_our_pack_size);
                $remaining_pieces = fmod($new_total_pieces,$new_our_pack_size);
                update_post_meta($variation_id, 'wcim_supplier_remaining_pieces', $remaining_pieces);
                update_post_meta($variation_id, 'wcim_supplier_total_pieces', $new_total_pieces);
                update_post_meta($variation_id, '_stock', $stock);
            }
            else if($previous_stock != $new_stock || 
               ($previous_stock != $new_stock && $previous_pack_size != $new_our_pack_size)){
                $total_pieces =  floor($new_stock*$new_our_pack_size);
                update_post_meta($variation_id, 'wcim_supplier_remaining_pieces', 0);
                update_post_meta($variation_id, 'wcim_supplier_total_pieces', $total_pieces);
                update_post_meta($variation_id, '_stock', $new_stock);
            }else if($previous_total_pieces != $new_total_pieces || 
                ($previous_pack_size != $new_our_pack_size) ||
                ($previous_total_pieces != $new_total_pieces && $previous_pack_size != $new_our_pack_size)){
                $stock = floor($new_total_pieces/$new_our_pack_size);
                $remaining_pieces = fmod($new_total_pieces,$new_our_pack_size);
                update_post_meta($variation_id, 'wcim_supplier_remaining_pieces', $remaining_pieces);
                update_post_meta($variation_id, 'wcim_supplier_total_pieces', $new_total_pieces);
                update_post_meta($variation_id, '_stock', $stock);
            }
        }

        function get_data($key) {
            $data = get_post_meta($this->product_id, $key, true);
            if (!$data) {
                $data = get_post_meta($this->parent_id, $key, true);
            }
            return $data;
        }

    }

    new WooICP_Supplier();
}
