<?php

if (!class_exists('WooICP_Order')) {

    class WooICP_Order {

        function __construct() {
            $wooci_order = get_option('wcim_order');
            if (!$wooci_order) {
                $this->install();
            }
            add_action('init', array($this, 'download_order_file'));
            add_action('init', array($this, 'generate_stock_report'));
            add_action('wp_ajax_add_product_manually', array($this, 'add_product_manually'));
            add_action('wp_ajax_create_product_file', array($this, 'create_product_file'));
            add_action('wp_ajax_complete_product_order', array($this, 'complete_product_order'));
            add_action('generate_stock_report', array($this, 'generate_report'));
        }

        function generate_stock_report() {
            if (!wp_next_scheduled('schedule_report_event')) {
                $schedule_time = getCurrentDateByTimeZone('Y-m-d') . ' 23:59:59';
                $schedule_time = get_gmt_from_date($schedule_time, 'U');
                wp_schedule_event($schedule_time, 'daily', 'generate_stock_report');
            }
        }
        public function get_used_currency(){
		global $wpdb;
		$sql = "SELECT meta_value AS currency FROM {$wpdb->prefix}postmeta WHERE meta_key = 'wcim_supplier_currency' GROUP BY meta_value";
		$result = $wpdb->get_results( $sql , ARRAY_A);
		return $result;
	}
        function generate_report() {
            global $wpdb;
            if (date('j') == 1) {
                $stock_data = get_option('wcim_stock_data');
                $year = date('Y', strtotime("-1 day"));
                $month = date('n', strtotime("-1 day"));
                $sql = "SELECT *  FROM `{$wpdb->prefix}postmeta` WHERE `meta_key` LIKE '%wcim_supplier_id%'";
                $product_list = $wpdb->get_results($sql);
                $data = array();
                
                $currency = $this->get_used_currency();
                $stock_data[$year][$month] = array();
                $default_our_pack_size = get_option('wcim_default_our_pack_size');
                if ($product_list) {
                    foreach ($product_list as $product_row) {
                        $id = $product_row->post_id;
                        $supplier_id = $product_row->meta_value;
                        $arrvived_stock = get_post_meta($id, '_stock', true);
                        $our_pack_size      = get_post_meta( $id, 'wcim_our_pack_size', true );
                        $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
                        $supplier_remaining_pieces = get_post_meta($id, 'wcim_supplier_remaining_pieces', true);
                        $total_pieces = floor($arrvived_stock*$our_pack+$supplier_remaining_pieces);
                        $product = get_post($id);
                        $purchase_price = get_post_meta($id, 'wcim_supplier_purchase_price', true);
                        $purchase_currency = get_post_meta($supplier_id, 'wcim_supplier_currency', true);
                        $purchase_price = $purchase_price ? $purchase_price : 0;
                        if (!$purchase_price && $product->post_parent) {
                            $purchase_price = get_post_meta($product->post_parent, 'wcim_supplier_purchase_price', true);
                            $purchase_price = $purchase_price ? $purchase_price : 0;
                        }
                        
                        if ($currency) {                            
                            foreach ($currency as $curr) {
                                if ($curr['currency'] == $purchase_currency) {
                                    $curr = $curr['currency'];
                                    $previous_amount = 0;
                                    if (is_array($stock_data) && isset($stock_data[$year][$month][$curr])) {
                                        $previous_amount = $stock_data[$year][$month][$curr];
                                    }
                                    $stock_data[$year][$month][$curr] = $previous_amount + ( $purchase_price * $total_pieces );
                                } 
                                else {
                                    $curr = $curr['currency'];
                                    if (!isset($stock_data[$year][$month][$curr])) {
                                        $stock_data[$year][$month][$curr] = 0;
                                    }
                                }
                            }
                        }
                    }
                    update_option('wcim_stock_data', $stock_data);
                }
            }
        }

        public function install() {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';

            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE IF NOT EXISTS $table_name (
						id mediumint(9) NOT NULL AUTO_INCREMENT,
						product_id mediumint(9) NOT NULL,
						supplier_id mediumint(9) NOT NULL,
						stock mediumint(5) NOT NULL,
						requested_stock mediumint(5) NOT NULL,
						arrvived_stock VARCHAR(5) NULL,
						order_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
						PRIMARY KEY (id)
						) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta($sql);
            update_option('wcim_order', 1);
        }

        public static function get_remaining_orders() {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $sql = "SELECT *,COUNT(supplier_id) AS total_products FROM `{$table_name}` WHERE arrvived_stock = '' GROUP BY order_date, supplier_id ORDER BY order_date ASC";
            $data_result = $wpdb->get_results($sql);
            return $data_result;
        }

        public static function get_total_products($where) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $sql = "SELECT COUNT(supplier_id) AS total_products FROM `{$table_name}` WHERE supplier_id = {$where['supplier_id']} AND order_date = '{$where['order_date']}'";
            $data_result = $wpdb->get_var($sql);
            return $data_result;
        }

        public static function get_total_purchase_amount($where) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $sql = "SELECT * FROM `{$table_name}` WHERE supplier_id = {$where['supplier_id']} AND order_date = '{$where['order_date']}'";
            $data_result = $wpdb->get_results($sql);
            $total_price = 0;

            foreach ($data_result as $row) {
                $product_id = $row->product_id;
                $requested_stock = $row->requested_stock;
                $supplier_pack_size = get_post_meta($product_id, 'wcim_supplier_pack_size', true);
                $total_requested_stock = $requested_stock * $supplier_pack_size;
                $product_price = get_post_meta($product_id, 'wcim_supplier_purchase_price', true);
                if ($product_price) {
                    $total_price += ( $product_price * $total_requested_stock );
                }
            }
            $supplier_currency = get_post_meta($where['supplier_id'], 'wcim_supplier_currency', true);
            $symbol = get_woocommerce_currency_symbol($supplier_currency);
            if ($symbol) {
                $symbol = " $symbol";
            }
            return number_format($total_price) . $symbol;
        }

        public static function get_ordered_product($supplier_id = 0) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $sql = "SELECT product_id FROM `{$table_name}` WHERE arrvived_stock = '' ";
            if ($supplier_id) {
                $sql .= ' AND supplier_id = ' . $supplier_id;
            }
            $data_result = $wpdb->get_results($sql);
            $product_list = array();
            if ($data_result) {
                foreach ($data_result as $data) {
                    $product_list[] = $data->product_id;
                }
            }
            return $product_list;
        }

        public static function get_ordered_supplier() {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $sql = "SELECT supplier_id FROM `{$table_name}` WHERE arrvived_stock = '' ";
            $data_result = $wpdb->get_results($sql);
            $supplier_list = array();
            if ($data_result) {
                foreach ($data_result as $data) {
                    $short_name = get_post_meta($data->supplier_id,'wcim_supplier_short_name',true);
                    $supplier_list[$data->supplier_id] = $short_name ? $short_name : get_the_title($data->supplier_id);
                }
            }
            return $supplier_list;
        }

        function check_product($product_id) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $sql = "SELECT count(id) FROM `{$table_name}` WHERE arrvived_stock = '' AND product_id ={$product_id}";
            $has_request = $wpdb->get_var($sql);
            return $has_request;
        }

        function insert($data) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $wpdb->insert(
                    $table_name, $data
            );
            return $wpdb->insert_id;
        }

        function update($data, $where) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'order_inventory';
            $total_rows = $wpdb->update(
                    $table_name, $data, $where
            );
            return $total_rows;
        }

        function complete_product_order() {
            global $wpdb;
            $message = $error = array();
            $message['success'] = false;
            $product_id = isset($_POST['product_id']) ? $_POST['product_id'] : 0;
            $requested_qty = isset($_POST['qty']) ? $_POST['qty'] : 0;
            $id = $_POST['id'];
            $message['row_id'] = $id;
            if ($product_id) {
                $requested_qty = $wpdb->get_var("SELECT requested_stock FROM {$wpdb->prefix}order_inventory WHERE id={$id} AND product_id={$product_id} AND arrvived_stock = '' ");
            } else {
                $product_id = $wpdb->get_var("SELECT product_id FROM {$wpdb->prefix}order_inventory WHERE id={$id}");
            }
            $supplier_pack_size = get_post_meta($product_id, 'wcim_supplier_pack_size',true);
            $default_supplier_pack = get_option('wcim_default_supplier_pack_size');
            $supplier_pack = $supplier_pack_size ? $supplier_pack_size : ($default_supplier_pack ? $default_supplier_pack : 1);
            $our_pack_size = get_post_meta($product_id,'wcim_our_pack_size',true);
            $default_our_pack = get_option('wcim_default_our_pack_size');
            $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack ? $default_our_pack : 1);
            $supplier_previous_total_pieces = get_post_meta($product_id, 'wcim_previous_total_pieces', true);
            if ($requested_qty || $product_id) {
                $result = $this->update(array('arrvived_stock' => $requested_qty), array('id' => $id));
                $new_total_pieces = floor($requested_qty * $supplier_pack+$supplier_previous_total_pieces);
                if ($result === false) {
                    $message['message'] = __('Ops! Something is wrong. Please try again.', 'wooic');
                } else if ($result) {
                    $message['success'] = true;
                    if ($requested_qty) {
                        $total_stock = floor($new_total_pieces/$our_pack);
                        $remaining_pieces = fmod($new_total_pieces, $our_pack);
                        update_post_meta($product_id, '_stock', $total_stock);
                        update_post_meta($product_id,'wcim_supplier_total_pieces',$new_total_pieces);
                        update_post_meta($product_id,'wcim_previous_total_pieces',$new_total_pieces);
                        update_post_meta($product_id,'wcim_supplier_remaining_pieces',$remaining_pieces);
                    }
                    $remaning_orders = WooICP_MAIN::pending_order_list();
                    $message['orders'] = $remaning_orders;
                    $message['message'] = __('Stock updated successfully!', 'wooic');
                } else {
                    $message['message'] = __('No row is updated!', 'wooic');
                }
            } else {
                $message['message'] = __('No product is found!', 'wooic');
            }
            $message['default'] = sprintf('<tr><td colspan="12">%s</td></tr>', __('No products are found!', 'wooic'));
            echo json_encode($message);
            wp_die();
        }

        function create_product_file() {
            $message = $error = array();
            $message['success'] = false;
            if (!isset($_POST['wooicp_product_nonce']) || !wp_verify_nonce($_POST['wooicp_product_nonce'], 'wooicp_create_product')) {
                $message['message'] = __('Ops! Something is wrong. Please try again.', 'wooic');
            } else {
                $order_date = date('Y-m-d H:i:s');
                $total_product_count = 0;
                if (isset($_POST['product']) && is_array($_POST['product']) && count($_POST['product'])) {
                    $order_data = array();
                    $ordered_product_id = array();
                    foreach ($_POST['product'] as $key => $data) {
                        if ($data['qty']) {
                            $order_data = array(
                                'product_id' => $key,
                                'stock' => $data['stock'],
                                'supplier_id' => $data['supplier'],
                                'requested_stock' => $data['qty'],
                                'arrvived_stock' => '',
                                'order_date' => $order_date
                            );
                            $result = $this->insert($order_data);
                            if ($result) {
                                $total_product_count++;
                                $ordered_product_id[] = $key;
                            }
                        }
                    }

                    $message['product_id'] = $ordered_product_id;
                    if ($total_product_count == count($_POST['product'])) {
                        $remaning_orders = WooICP_MAIN::pending_order_list();
                        $message['orders'] = $remaning_orders;
                        $message['success'] = true;
                        $message['message'] = __('Purchase order file generated successfully!', 'wooic');
                        $message['default'] = sprintf('<tr><td colspan="12">%s</td></tr>', __('No products are found!', 'wooic'));
                    } else if ($total_product_count > 0) {
                        $product_message = sprintf(__('Purchase order file generated for %d product!', 'wooic'),$total_product_count);
                        if ($total_product_count == 1) {
                            $product_message = sprintf(__('Purchase order file generated for %d product!', 'wooic'),$total_product_count);
                        }

                        $remaning_orders = WooICP_MAIN::pending_order_list();
                        $message['orders'] = $remaning_orders;
                        $message['message'] = sprintf($product_message, $total_product_count);
                        $message['default'] = sprintf('<tr><td colspan="12">%s</td></tr>', __('No products are found!', 'wooic'));
                        $message['success'] = true;
                    } else if ($total_product_count == 0) {
                        $message['message'] = __('No purchase order file was created!', 'wooic');
                    } else {
                        $message['message'] = __('Ops! Something is wrong. Please try again.', 'wooic');
                    }
                } else {
                    $message['message'] = __('Ops! Something is wrong. Please try again.', 'wooic');
                }
            }
            echo json_encode($message);
            wp_die();
        }

        public function add_product_manually() {
            $message = $error = array();
            $message['success'] = false;
            if (!isset($_POST['wooicp_nonce']) || !wp_verify_nonce($_POST['wooicp_nonce'], 'wooicp_add_product')) {
                $message['message'] = __('Ops! Something is wrong. Please try again.', 'wooic');
            } else {
                global $wpdb;
                $product_sku = $_POST['product_sku'];
                $product_qty = $_POST['qty'];
                if (empty($product_sku)) {
                    $error[] = __('Please enter product SKU.', 'wooic');
                }
                if (empty($product_qty)) {
                    $error[] = __('Please enter product QTY.', 'wooic');
                }
                if (count($error)) {
                    $message['message'] = implode("\n", $error);
                } else {
                    $sql = "SELECT post_id FROM `{$wpdb->postmeta}` WHERE meta_key LIKE '_sku' AND meta_value = '{$product_sku}'";
                    $product_id = $wpdb->get_var($sql);

                    if ($product_id) {
                        $check_product = $this->check_product($product_id);
                        if ($check_product) {
                            $message['message'] = __('This product is already being added.', 'wooic');
                        } else {
                            $id = $product_id;
                            $post = get_post($id);
                            $product_supplier = get_post_meta($id, 'wcim_supplier_product_url', true);
                            $product_stock = get_post_meta($id, '_stock', true);
                            $product_sku = get_post_meta($id, '_sku', true);
                            $warning_level = get_post_meta($id, 'wcim_supplier_warning_level', true);
                            $purchase_price = get_post_meta($id, 'wcim_supplier_purchase_price', true);
                            $product_supplier_id = get_post_meta($id, 'wcim_supplier_id', true);
                            $purchase_currency = get_post_meta($product_supplier_id, 'wcim_supplier_currency', true);
                            $purchase_price = $purchase_price ? $purchase_price : 0;
                            $product_type = $post->post_type;
                            $product_title = $product_type == 'product' ? get_the_title($id) : get_the_title($post->post_parent);
                            $actual_product_id = $product_type == 'product' ? $id : $post->post_parent;
                            $product_variant = '-';
                            $image = '<img width="40" height="40" src="' . plugins_url('/woocommerce/assets/images/placeholder.png') . '" />';
                            if ($product_type == 'product_variation') {
                                $product = new WC_Product_Variation($id);
                                $product_variant = $product->get_variation_attributes();
                                if (is_array($product_variant) && count($product_variant)) {
                                    $product_variant = implode(' | ', $product_variant);
                                } else {
                                    $product_variant = '-';
                                }
                                if (has_post_thumbnail($id)) {
                                    $image = get_the_post_thumbnail($id, array(40, 40));
                                } else {
                                    if (has_post_thumbnail($post->post_parent)) {
                                        $image = get_the_post_thumbnail($post->post_parent, array(40, 40));
                                    }
                                }
                            } else {
                                if (has_post_thumbnail($id)) {
                                    $image = get_the_post_thumbnail($id, array(40, 40));
                                }
                            }
                            if ($product_supplier) {
                                $product_supplier = sprintf('<a target="_blank" href="%s">%s</a>', $product_supplier, __('URL to product', 'wooic'));
                            } else {
                                $product_supplier = '-';
                            }

                            $warning_level = $warning_level ? $warning_level : 0;
                            $product_stock = $product_stock ? $product_stock : 0;
                            $purchase_price = $purchase_price . ' ' . get_woocommerce_currency_symbol($purchase_currency);
                            $product_title = sprintf('<a href="%s" >%s</a>', get_edit_post_link($actual_product_id), $product_title);

                            $content = '<tr>
									<td class="image column-thumb has-row-actions column-primary" data-colname="Image">%s</td>
									<td class="product_sku column-product_sku" data-colname="SKU">%s</td>
									<td class="product_name column-product_name" data-colname="Product name">%s</td>
									<td class="product_variant column-product_variant" data-colname="Product variant">%s</td>
									<td class="product_supplier column-product_supplier" data-colname="Supplier">%s</td>
									<td class="warning_level column-warning_level" data-colname="Warning level">%s</td>
									<td class="product_stock column-product_stock" data-colname="Stock">%s</td>
									<td class="purchase_price column-purchase_price" data-colname="Price">%s</td>
									<td class="amount column-amount" data-colname="Quantity">
									<input type="text"  data-id="%d"  name="product[%d][qty]" value="%d" />
									<input type="hidden" name="product[%d][stock]" value="%d" />
									<input type="hidden" name="product[%d][supplier]" value="%d" /></td>
								</tr>';
                            $message['success'] = true;
                            $message['id'] = $id;
                            $message['message'] = __('This product is already added!', 'wooic');
                            $message['data'] = sprintf($content, $image, $product_sku, $product_title, $product_variant, $product_supplier, $warning_level, $product_stock, $purchase_price, $id, $id, $product_qty, $id, $product_stock, $id, $product_supplier_id);
                        }
                    } else {
                        $message['message'] = __('No product has this SKU, please check and try again.', 'wooic');
                    }
                }
            }
            echo json_encode($message);
            wp_die();
        }

        public function download_order_file() {

            if (isset($_GET['action']) && $_GET['action'] == 'download_order_file') {
                global $wpdb;
                $supplier_id = $_GET['supplier'];
                $order_date = date('Y-m-d H:i:s', $_GET['date']);
                $sql = "SELECT * FROM `{$wpdb->prefix}order_inventory` WHERE supplier_id = {$supplier_id} AND order_date = '{$order_date}'";
                $result = $wpdb->get_results($sql);
                if (is_array($result) && count($result)) {
                    if (!class_exists('PHPExcel')) {
                        include( WOOICP_INCLUDES . '/PHPExcel.php' );
                    }

                    $objPHPExcel = new PHPExcel();

                    $objPHPExcel->getProperties()->setCreator("Woocommerce inventory management")
                            ->setLastModifiedBy("Woocommerce inventory management")
                            ->setTitle("Woocommerce inventory management")
                            ->setSubject("Woocommerce inventory management")
                            ->setDescription("Order details");

                    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri')
                            ->setSize(12);

                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A1', __('Product URL', 'wooic'))
                            ->setCellValue('B1', __('Art. ID', 'wooic'))
                            ->setCellValue('C1', __('Notes', 'wooic'))
                            ->setCellValue('D1', __('Our product name', 'wooic'))
                            ->setCellValue('E1', __('Our variant name', 'wooic'))
                            ->setCellValue('F1', __('QTY', 'wooic'))
                            ->setCellValue('G1', __('Our pack size', 'wooic'));

                    $objPHPExcel->getActiveSheet()->getStyle('A1')
                            ->applyFromArray(array('font' => array('bold' => true)));
                    $objPHPExcel->getActiveSheet()->getStyle('B1')
                            ->applyFromArray(array('font' => array('bold' => true)));
                    $objPHPExcel->getActiveSheet()->getStyle('C1')
                            ->applyFromArray(array('font' => array('bold' => true)));
                    $objPHPExcel->getActiveSheet()->getStyle('D1')
                            ->applyFromArray(array('font' => array('bold' => true)));
                    $objPHPExcel->getActiveSheet()->getStyle('E1')
                            ->applyFromArray(array('font' => array('bold' => true)));
                    $objPHPExcel->getActiveSheet()->getStyle('F1')
                            ->applyFromArray(array('font' => array('bold' => true)));
                    $objPHPExcel->getActiveSheet()->getStyle('G1')
                            ->applyFromArray(array('font' => array('bold' => true)));

                    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                    $alphas = range('A', 'Z');
                    $rowcount = 2;

                    $company_name = get_option('blogname');
                    $delivery_address = get_post_meta($supplier_id, 'wcim_supplier_address', true);
                    $company_phoneno = get_post_meta($supplier_id, 'wcim_supplier_phone_no', true);
                    $company_email = get_post_meta($supplier_id, 'wcim_supplier_email', true);
                    $supplier_currency = get_post_meta($supplier_id, 'wcim_supplier_currency', true);
                    $symbol = get_woocommerce_currency_symbol($supplier_currency);
                    if ($symbol) {
                        $symbol = " $symbol";
                    }
                    $default_our_pack_size = get_option('wcim_default_our_pack_size');
                    $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
                    foreach ($result as $row) {
                        $product = get_post($row->product_id);
                        $product_type = $product->post_type;
                        $product_title = $product->post_parent ? get_the_title($product->post_parent) : get_the_title($product->ID);
                        $product_variant = '-';
                        if ($product_type == 'product_variation') {
                            $variable_product = new WC_Product_Variation($product->ID);
                            $product_variant = $variable_product->get_variation_attributes();
                            if (is_array($product_variant) && count($product_variant)) {
                                $product_variant = implode(' | ', $product_variant);
                            } else {
                                $product_variant = '-';
                            }
                        }
                        $our_pack_size = get_post_meta( $product->ID , 'wcim_our_pack_size', true);
                        $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
                        $product_supplier = get_post_meta($product->ID, 'wcim_supplier_product_url', true);
                        $product_artid = get_post_meta($product->ID, 'wcim_supplier_art_id', true);
                        $product_notes = get_post_meta($product->ID, 'wcim_supplier_note', true);
                        $warning_level = get_post_meta($product->ID, 'wcim_supplier_warning_level', true);
                        $purchase_price = get_post_meta($product->ID, 'wcim_supplier_purchase_price', true);

                        $supplier_pack_size = get_post_meta($product->ID, 'wcim_supplier_pack_size', true);
                        $supplier_pack = $supplier_pack_size ? $supplier_pack_size : ($default_supplier_pack_size ? $default_supplier_pack_size : 1);
                        $product_total_requested_stock = $row->requested_stock * $supplier_pack;
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A' . $rowcount, empty($product_supplier) ? '-' : $product_supplier )
                                ->setCellValue('B' . $rowcount, empty($product_artid) ? '-' : $product_artid )
                                ->setCellValue('C' . $rowcount, empty($product_notes) ? '-' : $product_notes )
                                ->setCellValue('D' . $rowcount, empty($product_title) ? '-' : $product_title )
                                ->setCellValue('E' . $rowcount, empty($product_variant) ? '-' : $product_variant )
                                ->setCellValue('F' . $rowcount, $product_total_requested_stock)
                                ->setCellValue('G' . $rowcount, $our_pack);
                        $rowcount++;
                    }
                    $rowcount++;
                    $im_receiver = get_option('wcim_receiver');
                    $im_address1 = get_option('wcim_receiver_address1');
                    $im_address2 = get_option('wcim_receiver_address2');
                    $im_city = get_option('wcim_receiver_city');
                    $im_state = get_option('wcim_receiver_state');
                    $im_zip_code = get_option('wcim_receiver_zip_code');
                    $im_contact = get_option('wcim_receiver_contact');
                    $im_country = get_option('wcim_receiver_country');
                    
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount, "Shipping address");
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount++)
                            ->applyFromArray(array('font' => array('bold' => true)));
                    if (!empty($im_receiver)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("Receiver : ".$im_receiver));
                    }
                    if (!empty($im_contact)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("Contact person : ".$im_contact));
                    } 
                    if (!empty($im_address1)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("Address line1 : ".$im_address1));
                    }
                    if (!empty($im_address2)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("Address line2 : ".$im_address2));
                    }
                    if (!empty($im_city)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("City : ".$im_city));
                    }
                    if (!empty($im_state)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("State / Province / Region : ".$im_state));
                    }
                    if (!empty($im_zip_code)) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("Zip / Postal code : ".$im_zip_code));
                    } 
                    if (!empty($im_country)) {
                        $im_country_name = WC()->countries->countries[$im_country];
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $rowcount++, html_entity_decode("Country : ".$im_country_name));
                    }
                    foreach (range('A', 'G') as $columnID) {
                        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                                ->setAutoSize(true);
                    }


                    $order_date = str_replace(array(' ', ':'), '-', $order_date);

                    $filename = $supplier_id . '_' . $order_date . '.xls';
                    if (!class_exists('PHPExcel_IOFactory')) {
                        include_once( WOOICP_INCLUDES . '/PHPExcel/IOFactory.php' );
                    }
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment; filename=\"$filename\"");
                    header("Cache-Control: max-age=0");

                    $objWriter->save('php://output');
                }
            }
        }
    }

    new WooICP_Order();
}
?>