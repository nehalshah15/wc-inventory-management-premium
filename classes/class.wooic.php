<?php

if (!class_exists('WooICP')) {

    class WooICP {

        function __construct() {
            if ($this->validate_dependencies()) {
                $this->load_files();
            } else {
                /* Display dependency error */
                add_action('admin_notices', function () {
                    $arrActivePlugins = apply_filters('active_plugins', get_option('active_plugins'));

                    include_once( WOOICP_TEMPLATE . 'dependency_error.php' );
                });
            }
        }

        function validate_dependencies() {
            $arrActivePlugins = apply_filters('active_plugins', get_option('active_plugins'));

            if (!in_array('woocommerce/woocommerce.php', $arrActivePlugins)) {
                return false;
            }
            return true;
        }

        function load_files() {
            global $woocommerce;
            require_once( WOOICP_INCLUDES . 'functions.php' );
            require_once( WOOICP_CLASSES . 'class.im_main.php' );
            require_once( WOOICP_CLASSES . 'class.order.php' );
            require_once( WOOICP_CLASSES . 'class.supplier.php' );
        }
    }

    new WooICP();
}