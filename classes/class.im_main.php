<?php
if (!class_exists('WooICP_MAIN')) {

    class WooICP_MAIN {

        function __construct() {
            add_action('init', array($this, 'supplier_init'), 6);
            add_action('add_meta_boxes', array($this, 'supplier_additional_info'));
            add_action('admin_enqueue_scripts', array($this, 'plugin_scripts'));
            add_action('save_post', array($this, 'save_supplier'));
            add_action('admin_menu', array($this, 'inventory_management_menu'),5);
            add_filter('manage_posts_columns', array($this, 'supplier_add_id_column'), 5);
            add_action('manage_posts_custom_column', array($this, 'supplier_add_id_column_content'), 5, 2);
            // Purchase license
            add_action('init', array($this, 'verify_purchased_licence'), 1);
            // stock management
            add_action('woocommerce_single_product_summary', array($this, 'display_pcs_per_product_in_single_page'), 18);
            add_action('woocommerce_before_variations_form', array($this, 'change_our_pack_size_on_variation_change'));
            add_action('init', array($this, 'save_inventory_settings'), 1);
            add_action('wp_ajax_get_all_variation_json',array($this,'get_all_variation_json'));
			add_action('wp_ajax_nopriv_get_all_variation_json',array($this,'get_all_variation_json'));
            add_action('wp_ajax_save_unit_to_meta',array($this,'save_unit_to_meta'));
            add_action('wp_ajax_delete_unit_from_meta',array($this,'delete_unit_from_meta'));
            add_action('init',array($this,'add_database_updation_notice'));
            add_action('wp_ajax_get_product_data_to_update',array($this,'get_product_data_to_update'));
            add_action('wp_ajax_add_default_data_to_existing_product',array($this,'add_default_data_to_existing_product'));
            add_action('admin_menu', array($this, 'add_stock_values_menu'),10);
            
            //order status wc hooks
            add_action( 'woocommerce_order_status_cancelled', array($this,'restore_product_stock_callback') );
			add_action( 'woocommerce_order_status_failed', array($this,'restore_product_stock_callback') );
			add_action( 'trash_shop_order', array($this,'restore_product_stock_callback') );
			add_action( 'woocommerce_order_status_processing', array($this,'restore_product_stock_from_cancel_to_processing_callback') );
			add_action( 'woocommerce_order_status_completed', array($this,'restore_product_stock_from_cancel_to_processing_callback') );
			add_action( 'woocommerce_order_status_on-hold', array($this,'restore_product_stock_from_cancel_to_processing_callback') );
			add_action( 'publish_shop_order', array($this,'restore_product_stock_from_cancel_to_processing_callback') );
			add_action( 'woocommerce_before_save_order_items', array($this,'update_wc_stock_after_edit_qty'), 10, 2 );
			add_action( 'woocommerce_before_delete_order_item', array($this,'update_stock_after_delete_order_item') ); 
			add_action( 'woocommerce_ajax_add_order_item_meta', array($this,'update_stock_after_add_order_item'), 10, 2 ); 
            
        }
         // Add Inventory Management Menu
        function inventory_management_menu() {
            $license_verified = get_option('wcim_license_verified');
            if ($license_verified) {
                add_menu_page(__('Inventory management','wooic'),
                    __('Inventory management','wooic'),
                    'manage_options',
                    'wc-inventory-management',
                    '',
                    'dashicons-list-view',50);
                add_submenu_page('wc-inventory-management', __('Stock levels', 'wooic'), __('Stock levels', 'wooic'), 'view_woocommerce_reports', 'wc-inventory-management', array($this, 'inventory_management'));
                add_submenu_page('wc-inventory-management', __('Settings','wooic'), __('Settings','wooic'), 'manage_options', 'wc-inventory-management-settings', array($this, 'purchase_license'));                
            }
            if($license_verified != true && !is_plugin_active('wc-inventory-management/wc-inventory-management.php')){
                add_menu_page(__('Inventory management','wooic'),
                    __('Inventory management','wooic'),
                    'manage_options',
                    'wc-inventory-management-settings',
                    '',
                    'dashicons-list-view',50);
                add_submenu_page('wc-inventory-management', __('Settings','wooic'), __('Settings','wooic'), 'manage_options', 'wc-inventory-management-settings', array($this, 'purchase_license'));
            }
        }

        // Register Supplier
        function supplier_init() {

            $labels = array(
                'name' => _x('Suppliers', 'Post type general name', 'wooic'),
                'singular_name' => _x('Supplier', 'Post type singular name', 'wooic'),
                'menu_name' => __('Suppliers', 'wooic'),
                'name_admin_bar' => __('Supplier', 'wooic'),
                'all_items' => __('Suppliers', 'wooic'),
                'add_new_item' => __('Add new supplier', 'wooic'),
                'add_new' => __('Add new supplier', 'wooic'),
                'new_item' => __('New supplier', 'wooic'),
                'edit_item' => __('Edit supplier', 'wooic'),
                'update_item' => __('Update supplier', 'wooic'),
                'view_item' => __('View supplier', 'wooic'),
                'view_items' => __('View suppliers', 'wooic'),
                'search_items' => __('Search supplier', 'wooic'),
                'not_found' => __('Not found', 'wooic'),
                'not_found_in_trash' => __('Not found in Trash', 'wooic'),
            );
            $args = array(
                'label' => __('Supplier', 'wooic'),
                'labels' => $labels,
                'supports' => array('title'),
                'hierarchical' => false,
                'public' => false,
                'show_ui' => true,
                'show_in_menu' => 'wc-inventory-management',
                'exclude_from_search' => true,
                'publicly_queryable' => false,
                'capability_type' => 'page',
                'map_meta_cap' => true,
                'rewrite' => false,
                'query_var' => false,
                'show_in_nav_menus' => false,
                'show_in_admin_bar' => true,
            );
            $license_verified = get_option('wcim_license_verified');
            if ($license_verified) {
                register_post_type('supplier', $args);
            }
        }

        function supplier_add_id_column_content($column, $id) {
            if ($_GET['post_type'] == 'supplier') {
                wp_enqueue_style('wooicp_style');
                if ($column == 'supplier_id') {
                    echo $id;
                }?>
                <style> .bulkactions{ display: none;}#cb,.check-column{display: none;}</style>
                <?php
            }
        }

        function supplier_add_id_column($columns) {
            if ($_GET['post_type'] == 'supplier') {
                $newcolumns['cb'] = $columns['cb'];
                $newcolumns['supplier_id'] = 'ID';
                $newcolumns['title'] = $columns['title'];
                $newcolumns['date'] = $columns['date'];
                $columns = $newcolumns;
            }
            return $columns;
        }

        // Supplier additional info
        function supplier_additional_info() {
            add_meta_box('supplier_meta_box', __('Supplier details', 'wooic'), array($this, 'supplier_details'), 'supplier', 'normal', 'high');
        }

        // Supplier HTML
        function supplier_details() {
            $post_id = get_the_ID();
            $countries = get_countries_list();
            wp_enqueue_style('wooicp_style');
            $supplier_short_name = get_post_meta($post_id, 'wcim_supplier_short_name', true);
            $supplier_address = get_post_meta($post_id, 'wcim_supplier_address', true);
            $supplier_country = get_post_meta($post_id, 'wcim_supplier_country', true);
            $supplier_website_url = get_post_meta($post_id, 'wcim_supplier_website_url', true);
            $supplier_order_url = get_post_meta($post_id, 'wcim_supplier_order_url', true);
            $supplier_currency = get_post_meta($post_id, 'wcim_supplier_currency', true);
            $default_supplier_currency = get_option('wcim_default_supplier_currency');
            if(!$supplier_currency){
                $supplier_currency = $default_supplier_currency;
            }  
            $supplier_description = get_post_meta($post_id, 'wcim_supplier_description', true);
            $supplier_email = get_post_meta($post_id, 'wcim_supplier_email', true);
            $supplier_order_email = get_post_meta($post_id, 'wcim_supplier_order_email', true);
            $supplier_skype_id = get_post_meta($post_id, 'wcim_supplier_skype_id', true);
            $supplier_phone_no = get_post_meta($post_id, 'wcim_supplier_phone_no', true);
            include_once( WOOICP_TEMPLATE . 'metabox_supplier.php' );
        }

        // Plugin Style and scripts
        function plugin_scripts() {
            wp_enqueue_style('wooicp_style', WOOICP_PLUGIN_URL . 'css/admin_style.css');
            wp_register_style('wooicp_font_awesome', WOOICP_PLUGIN_URL . 'css/fontawesome-all.min.css');
            wp_register_script('wooicp_script', WOOICP_PLUGIN_URL . 'js/scripts.js', array('jquery'));
            wp_enqueue_script('order_status_table-js', WOOICP_PLUGIN_URL . 'js/order_status_table.js', array('jquery'));
            $translation_array =  array(
                'cancel_text'=>__('Cancel','wooic'),
                'singular_ex'=>__('Ex. meter','wooic'),
                'plural_ex'=>__('Ex. meters','wooic'),
                'delete_text'=>__('Delete','wooic'),
                'confirm_msg' => __("Deleting a unit will set all products using this unit to the default unit, are you sure you want to delete this unit?",'wooic'),
            );
            wp_localize_script( 'wooicp_script', 'wooic_obj', $translation_array );
            wp_enqueue_script('wooicp_script');
            wp_enqueue_style('wooicp_font_awesome');
        }

        // Save supplier data
        function save_supplier($post_id) {
            if (wp_is_post_revision($post_id)) {
                return;
            }
            $post_type = get_post_type($post_id);

            // If this isn't a 'supplier' post, don't update it.
            if ("supplier" != $post_type) {
                return;
            }
            if (!isset($_POST['supplier_short_name'])) {
                return;
            }
            update_post_meta($post_id, 'wcim_supplier_short_name', $_POST['supplier_short_name']);
            update_post_meta($post_id, 'wcim_supplier_address', $_POST['supplier_address']);
            update_post_meta($post_id, 'wcim_supplier_country', $_POST['supplier_country']);
            update_post_meta($post_id, 'wcim_supplier_website_url', $_POST['supplier_website_url']);
            update_post_meta($post_id, 'wcim_supplier_order_url', $_POST['supplier_order_url']);
            update_post_meta($post_id, 'wcim_supplier_currency', $_POST['supplier_currency']);
            update_post_meta($post_id, 'wcim_supplier_description', $_POST['supplier_description']);
            update_post_meta($post_id, 'wcim_supplier_email', $_POST['supplier_email']);
            update_post_meta($post_id, 'wcim_supplier_order_email', $_POST['supplier_order_email']);
            update_post_meta($post_id, 'wcim_supplier_skype_id', $_POST['supplier_skype_id']);
            update_post_meta($post_id, 'wcim_supplier_phone_no', $_POST['supplier_phone_no']);
        }

        // Get supplier list
        static function get_supplier_list($default_supplier_list = false) {

            if ($default_supplier_list) {
                $args = array(
                    'post_type' => 'supplier',
                    'posts_per_page' => -1,
                    'meta_key'=>'wcim_supplier_short_name',
                    'orderby'=>'meta_value',
                    'order'=>'ASC'
                );

                $supplier_list = array(__('Select supplier', 'wooic'));
                $supplier_data = new WP_Query($args);
                if ($supplier_data->have_posts()) {
                    while ($supplier_data->have_posts()) {
                        $supplier_data->the_post();
                        $short_name = get_post_meta(get_the_ID(),'wcim_supplier_short_name',true);
                        $supplier_list[get_the_ID()] = $short_name ? $short_name : get_the_title();
                    }
                }

                wp_reset_postdata();
                return $supplier_list;
            }

            $args = array(
                'post_type' => array('product', 'product_variation'),
                'meta_query' => array(
                    'compare' => 'AND',
                    array(
                        'key' => '_stock',
                        'compare' => '<=',
                        'value' => 'mt1.meta_value',
                        'type' => 'NUMERIC'
                    ),
                    array(
                        'key' => 'wcim_supplier_warning_level',
                        'compare' => 'EXISTS'
                    ),
                    array(
                        'key' => 'wcim_supplier_id',
                        'compare' => '!=',
                        'value' => '',
                        'type' => 'NUMERIC'
                    ),
                ),
                'posts_per_page' => -1,
            );

            $ordered_product = WooICP_Order::get_ordered_product();
            if (is_array($ordered_product) && count($ordered_product)) {
                $args['post__not_in'] = $ordered_product;
            }

            add_filter('posts_request', array('WooICP_MAIN', 'closure'));
            $product_list = new WP_Query($args);
            remove_filter('posts_request', array('WooICP_MAIN', 'closure'));

            $supplier_list = array(__('Select supplier', 'wooic'));
            if ($product_list->have_posts()) {
                while ($product_list->have_posts()) {
                    $product_list->the_post();
                    $supplier_id = get_post_meta(get_the_ID(), 'wcim_supplier_id', true);
                    $short_name = get_post_meta($supplier_id,'wcim_supplier_short_name',true);
                    $supplier_list[$supplier_id] = $short_name ? $short_name : get_the_title($supplier_id);
                }
            }

            wp_reset_postdata();
            return $supplier_list;
        }

        // Handle page
        function inventory_management() {
            global $default_supplier;
            global $total_pagination_records;
            wp_enqueue_style('wooicp_style');
            $remaning_orders = WooICP_MAIN::pending_order_list();
            if (isset($_GET['subpage']) && $_GET['subpage'] == 'delivery_table') {
                $supplier_list = WooICP_Order::get_ordered_supplier();
                asort($supplier_list);
                $supplier_array = array_keys($supplier_list);
                if (is_array($supplier_array) && count($supplier_array)) {
                    $default_supplier = $supplier_array[0]; 
                }
                require_once( WOOICP_CLASSES . 'class.data_delivery_table.php' );
                $data = new DataDeliveryTable();
            } else {
                $supplier_list = WooICP_MAIN::get_supplier_list();
                unset($supplier_list[0]);
                asort($supplier_list);
                $all_supplier = array('all' =>'All');
                $supplier_list= $all_supplier + $supplier_list;
                $no_supplier = array(0 =>'No supplier');
                $supplier_list= $supplier_list + $no_supplier;
                $supplier_array = array_keys($supplier_list);
                if (is_array($supplier_array) && count($supplier_array)) {
                    foreach ( $supplier_array as $supplier ){
                            $count = WooICP_MAIN::total_low_stock_products($supplier);
                            if( $count && !$default_supplier ){
                                $default_supplier = $supplier;
                            }
                            if(isset($_GET['supplier_id']) && $_GET['supplier_id'] == $supplier && $_GET['supplier_id'] !== "all"){
                                $total_pagination_records = $count;
                            }else if($supplier === "all"){
                                $total_pagination_records = $count;
                            }
                        }
                    }
                    if($count <= 0){
                        unset($supplier_list['all']);
                        unset($supplier_list[0]);
                    }
                require_once( WOOICP_CLASSES . 'class.data_low_stock.php' );
                $data = new DataLowStock();
            }
            $data->prepare_items();
            include( WOOICP_TEMPLATE . 'inventory_management.php' );
        }

        // Handle new order list
        public static function pending_order_list() {
            $remaning_orders = WooICP_Order::get_remaining_orders();
            ob_start();
            include( WOOICP_TEMPLATE . 'order_files_list.php' );
            $content = ob_get_contents();
            ob_clean();
            return $content;
        }

        public static function closure($sql) {
            return str_replace("'mt1.meta_value'", "mt1.meta_value", $sql);
        }

        // Get no of low stock products supplier wise
        public static function total_low_stock_products($supplier_id) {
            global $wpdb;
            $sql = 'SELECT DISTINCT(post_parent) FROM '.$wpdb->posts.' WHERE post_parent > 0';
            $variable_products = $wpdb->get_col( $sql );
            if($supplier_id !== "all"){
                $meta_query = array(
                            'key' => 'wcim_supplier_id',
                            'compare' => '=',
                            'value' => $supplier_id,
                            'type' => 'NUMERIC'
                    );
            }else{
                $meta_query = array(
                            'key' => 'wcim_supplier_id',
                            'compare' => 'EXISTS',
                    );
            }
            $paged = isset($_REQUEST['paged']) ? $_REQUEST['paged'] : 1 ;
            if(isset($_GET['items_per_page'])){
                $perPage = $_GET['items_per_page'];
            }else{
                $perPage = 25;
            }
            $ordered_product = WooICP_Order::get_ordered_product();
            if( is_array( $ordered_product ) && count( $ordered_product ) ){
                $post_not_in = array_merge($variable_products,$ordered_product);
            }else{
                $post_not_in = $variable_products;
            }
            $args = array(
                'post_type' => array('product', 'product_variation'),
                'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => '_stock',
                            'compare' => '<=',
                            'value' => 'mt1.meta_value',
                            'type' => 'NUMERIC'
                        ),
                        array(
                            'key'	=> 'wcim_supplier_warning_level',
                            'compare'	=>	'EXISTS'
                        ),
                        array(
                            'key' => 'wcim_supplier_show_in_low_stock',
                            'compare' => '=',
                            'value' => "yes"
                        ),
                    $meta_query),
                'post_status' => array( 'private', 'publish' ),
                'posts_per_page' => $perPage,
                'paged'=> $paged,
                'post__not_in' => $post_not_in
            );
            add_filter('posts_request', array('WooICP_MAIN', 'closure'));
            $product_list = new WP_Query($args);
            $found_post = $product_list->found_posts;            
            remove_filter('posts_request', array('WooICP_MAIN', 'closure'));
            wp_reset_postdata(); 
            return $found_post;
        }
        function purchase_license() {
            wp_enqueue_style('wooicp_style');
            $units = get_option('wcim_units');
            if(!$units){
                $units =  array('piece'=>'pieces');
                update_option('wcim_units', $units);
            }
            $license_key = get_option('wcim_verified_purchase_license_key');
            $license_verified = get_option('wcim_license_verified');
            $default_unit = get_option('wcim_default_unit');
            $default_our_pack_size = get_option('wcim_default_our_pack_size');
            $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
            $default_supplier_currency = get_option('wcim_default_supplier_currency');
            $show_settings = get_option('wcim_show_default_pack_setting');
            $show_one_pack_settings = get_option('wcim_show_one_pack_settings');
            $im_receiver = get_option('wcim_receiver');
            $im_address1 = get_option('wcim_receiver_address1');
            $im_address2 = get_option('wcim_receiver_address2');
            $im_city = get_option('wcim_receiver_city');
            $im_state = get_option('wcim_receiver_state');
            $im_zip_code = get_option('wcim_receiver_zip_code');
            $im_contact = get_option('wcim_receiver_contact');
            $im_country = get_option('wcim_receiver_country');
            $countries = get_countries_list();
            $currencies = get_woocommerce_currencies();
            include( WOOICP_TEMPLATE . 'wooic_settings.php' );
        }

        function verify_purchased_licence() {
            if (isset($_POST['verify_purchase_key_btn']) && $_POST['verify_purchase_key_btn'] != '') {
                $flag = false;
                $response = $this->fnValidateTheLicense($_POST['purchase_key']);
                if ($response == "invalid") {
                    update_option('wcim_verified_purchase_license_key', $_POST['purchase_key']);
                    update_option('wcim_verified_purchase_license_date', "");
                    update_option('wcim_license_verified', $flag);
                    wp_redirect('admin.php?page=wc-inventory-management-settings&flag=0');
                    exit();
                } else {
                    $flag = true;
                    update_option('wcim_verified_purchase_license_key', $_POST['purchase_key']);
                    update_option('wcim_verified_purchase_license_date', date('Y-m-d'));
                    update_option('wcim_license_verified', $flag);
                    wp_redirect('admin.php?page=wc-inventory-management-settings&flag=1');
                    exit();
                }
            }
            if (isset($_GET['flag'])) {
                if ($_GET['flag'] == 1) {
                    add_action('admin_notices', array($this, 'purchase_license_success_message'));
                } else if ($_GET['flag'] == 0) {
                    add_action('admin_notices', array($this, 'invalid_purchase_license_notice'));
                }
            }
            $license_verified = get_option('wcim_license_verified');
            if ($license_verified) {
                $current_date = date_create(date('Y-m-d'));
                $license_purchase_verified_date = date_create(get_option('wcim_verified_purchase_license_date'));
                $interval = date_diff($license_purchase_verified_date, $current_date);
                $data_diff = $interval->format('%a');
                if ($data_diff > 7) {
                    $response = $this->fnValidateTheLicense(get_option('wcim_verified_purchase_license_key'));
                    if ($response == "invalid") {
                        $flag = false;
                        update_option('wcim_verified_purchase_license_key', $_POST['purchase_key']);
                        update_option('wcim_verified_purchase_license_date', "");
                        update_option('wcim_license_verified', $flag);
                        add_action('admin_notices', array($this, 'invalid_purchase_license_notice'));
                    }
                }
            }
        }

        function fnValidateTheLicense($purchase_key) {
            $result = false; // have we got a valid purchase code?
            $id = 16993804; // Item Id of the plugin provide by codecanyon
            $uname = 'prismitsystems'; // authors username on codecanyon
            $aKey = 'zyoao0dxm5h6if6tr5o7bijdrpu29rrk'; // api key from my account area in codecanyon       

            $strLiveURL = 'http://marketplace.envato.com/api/edge';
            $strLiveURL .= "/$uname/$aKey/verify-purchase:$purchase_key.json";

            $ch = curl_init($strLiveURL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $agent = 'REFFERAL-AGENT';
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            $json_res = curl_exec($ch);

            $data = json_decode($json_res, true);

            $purchases = $data['verify-purchase'];
            if (isset($purchases['buyer'])) {
                // format single purchases same as multi purchases
                $purchases = array($purchases);
            }
            $purchase_details = array();
            if (is_array($purchases) && count($purchases) > 0) {
                foreach ($purchases as $purchase) {
                    $purchase = (array) $purchase; // json issues
                    if ((int) $purchase['item_id'] == (int) $id) {
                        // we have a winner!
                        $result = true;
                        $purchase_details = $purchase;
                    }
                }
            }

            // do something with the users purchase details, 
            // eg: check which license they've bought, save their username something
            if ($result) {
                return $purchase_details;
            } else {
                return 'invalid';
            }
        }

        function invalid_purchase_license_notice() {
            ?>
                <div class="error"><p><strong><?php _e('Inventory management PREMIUM - License verification','wooic'); ?></strong></p>
                    <p><?php _e('The key you entered was not valid, please make sure you provide a valid purchase key!','wooic'); ?></p>
                    </div>
                <?php
        }

        function purchase_license_success_message() {
            ?>
            <div class="updated notice"><p><strong><?php _e('Inventory management PREMIUM - License verification','wooic'); ?></strong></p>
                    <p><?php _e('Your purchase license key has been successfully stored. Now you can explore PREMIUM version of inventory management','wooic'); ?></p>
                    </div>
                    <?php
        }

        function get_all_variation_json(){
            if(isset($_POST['product_id'])){
                $args = array(
                        'post_type'     => 'product_variation',
                        'post_status'   => array( 'private', 'publish' ),
                        'numberposts'   => -1,
                        'orderby'       => 'menu_order',
                        'order'         => 'asc',
                        'post_parent'   => $_POST['product_id']
                );
                $variations = get_posts( $args );
                $units = get_option('wcim_units');
                $data = array();
                $unit_has_varation = false;
                $unit_size = $pack_size = 0;
                $default_unit = get_option('wcim_default_unit');
                foreach ( $variations as $variation ) {
                        $variation_ID = $variation->ID;
                        $product_variation = new WC_Product_Variation( $variation_ID );
                        $data['variation_id'] = $variation_ID;
						$data['default_msg'] = '';
                        $data['attributes'] = $product_variation->attributes;
                        $our_pack_size = (int)get_post_meta($variation_ID, 'wcim_our_pack_size', true);
                        $parent_our_pack_size = (int)get_post_meta( $_POST['product_id'] , 'wcim_our_pack_size', true);
                        $default_our_pack_size = (int)get_option('wcim_default_our_pack_size');
                        $our_pack = $our_pack_size ? $our_pack_size : ($parent_our_pack_size ? $parent_our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1));
                        $selected_unit = get_post_meta($variation_ID, 'wcim_supplier_unit', true);
                        $parent_unit = get_post_meta($_POST['product_id'], 'wcim_supplier_unit', true);
                        if (!array_key_exists($selected_unit,$units))
                        {
                            if(!array_key_exists($parent_unit,$units)){
                                if(array_key_exists($default_unit,$units)){
                                    $unit = $default_unit;
                                }else{
                                    $unit="piece";  
                                }
                                
                            }else{
                                $unit = $parent_unit;
                            }  
                        }else{
                            $unit = $selected_unit;
                        }
                        $show_one_pack_settings = get_option('wcim_show_one_pack_settings');
                        if($our_pack == 1 && !$show_one_pack_settings){
                            $data['msg'] = sprintf( __( 'Sold per %s', 'wooic' ), __($unit,'wooic') );
                        }else if($our_pack >= 2){
                            $data['msg'] = sprintf( __( 'Sold in pack of %d %s', 'wooic' ), $our_pack, __($units[$unit],'wooic') );
                        }else{
                            $data['msg'] = "";
                            $data['default_msg'] = sprintf( __( 'Sold per %s', 'wooic' ), __($unit,'wooic') );
                        }
                        $arr[] = $data;
                        if( !$pack_size ){
                        	$pack_size = $our_pack;
							$unit_size = $unit;
                        }
                        if( $pack_size != $our_pack || $unit_size != $unit ){
                                $unit_has_varation = true;
                        }
                        
                }
                if( $unit_has_varation ){
                        $tempArr = $arr;
                        foreach( $tempArr as $key => $data ){
                                if( $data['default_msg'] != '' ){
                                        $arr[$key]['msg'] = $data['default_msg'];
                                }
                        }
                }
                echo json_encode($arr);
                die;
            }
        }
        
        function display_pcs_per_product_in_single_page() {
            global $product;
            $show_settings = get_option('wcim_show_default_pack_setting');
                if( $product->is_type('simple')){
                    $our_pack_size = (int)get_post_meta($product->id, 'wcim_our_pack_size', true);
                    $default_our_pack_size = (int)get_option('wcim_default_our_pack_size');
                    $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
                    $units = get_option('wcim_units');
                    $selected_unit = get_post_meta($product->id, 'wcim_supplier_unit', true);
                    $default_unit = get_option('wcim_default_unit');
                    $unit = "";
                    if (!array_key_exists($selected_unit,$units))
                    {
                        if(array_key_exists($default_unit,$units)){
                            $unit = $default_unit;
                        }else{
                            $unit="piece";
                        }
                    }else{
                        $unit = $selected_unit;
                    }
                    $show_one_pack_settings = get_option('wcim_show_one_pack_settings');
                    $manual_pack_size_setting = get_post_meta($product->id,'wcim_manual_pack_size_setting',true);
                    if($manual_pack_size_setting == 1 || ($manual_pack_size_setting == 0 && $show_settings == 1)){
                        if($our_pack){
                            if($our_pack == 1 && !$show_one_pack_settings){
                                $text = sprintf( __( 'Sold per %s', 'wooic' ), __($unit,'wooic') );
                            }else if($our_pack >= 2){
                                $text = sprintf( __( 'Sold in pack of %d %s', 'wooic' ), $our_pack,__($units[$unit],'wooic') );
                            }else{
                                $text = "";
                            }
                        }
                        echo '<div class="our_pack_size" data-product_variations="">'.$text.'</div>';
                    }
                }
                else if($product->is_type('variable')){
	                $args = array(
	                        'post_type'     => 'product_variation',
	                        'post_status'   => array( 'private', 'publish' ),
	                        'numberposts'   => -1,
	                        'orderby'       => 'menu_order',
	                        'order'         => 'asc',
	                        'post_parent'   => $product->id
	                );
	                $variations = get_posts( $args );
	                $units = get_option('wcim_units');
	                $data = array();
                        $unit_size = '';
                        $pack_size = 0;
                        $unit_has_varation = false;
                        $default_our_pack_size = (int)get_option('wcim_default_our_pack_size');
                        $default_unit = get_option('wcim_default_unit');
                        $show_one_pack_settings = get_option('wcim_show_one_pack_settings');
                        $unit="";
	                foreach ( $variations as $variation ) {
	                        $variation_ID = $variation->ID;
	                        $product_variation = new WC_Product_Variation( $variation_ID );
	                        $data['variation_id'] = $variation_ID;
	                        $data['attributes'] = $product_variation->attributes;
	                        $our_pack_size = (int)get_post_meta($variation_ID, 'wcim_our_pack_size', true);
	                        $parent_our_pack_size = (int)get_post_meta($product->id, 'wcim_our_pack_size', true);
	                        
	                        $our_pack = $our_pack_size ? $our_pack_size : ($parent_our_pack_size ? $parent_our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1));
	                        $selected_unit = get_post_meta($variation_ID, 'wcim_supplier_unit', true);
	                        $parent_unit = get_post_meta($product->id, 'wcim_supplier_unit', true);
	                        if (!array_key_exists($selected_unit,$units))
                                {
                                    if(!array_key_exists($parent_unit,$units)){
                                        if(array_key_exists($default_unit,$units)){
                                            $unit = $default_unit;
                                        }else{
                                            $unit="piece";  
                                        }
                                    }else{
                                        $unit = $parent_unit;
                                    } 
                                }else{
                                    $unit = $selected_unit;
                                }
	                        if( !$pack_size ){
	                        	$pack_size = $our_pack;
					$unit_size = $unit;
                                }
                                if( $pack_size != $our_pack || $unit_size != $unit ){
                                        $unit_has_varation = true;
                                }
	                }
                        $default_message = $message = __( 'Select variant to show pack-size', 'wooic' );
                        if( !$unit_has_varation && $pack_size == 1 && !$show_one_pack_settings ){
                            $message = sprintf( __( 'Sold per %s', 'wooic' ), __($unit_size,'wooic') );
                            $default_message = $message;
                        }else if( !$unit_has_varation && $pack_size >= 2){
                            $message = sprintf( __( 'Sold in pack of %d %s', 'wooic' ), $pack_size, __($units[$unit_size],'wooic') );
                            $default_message = $message;
                        }else if( !$unit_has_varation && $show_one_pack_settings ){
                            $message = '';
                            $default_message ='';
                        }
                        $manual_pack_size_setting = get_post_meta($product->id,'wcim_manual_pack_size_setting',true);
                        if($manual_pack_size_setting == 1 || ($manual_pack_size_setting == 0 && $show_settings == 1)){
                            echo '<div class="our_pack_size" data-default-message="'. $default_message .'" data-product_variations="">'.$message.'</div>';                        
                        }
                }
                ?>
                <script>
                    jQuery(document).ready(function () {
                       jQuery.ajax({
                            url: '<?php echo admin_url('admin-ajax.php') ?>',
                            type: 'POST',
                            dataType: 'json',
                            async: true,
                            data: {action: 'get_all_variation_json',product_id:<?php echo get_the_ID(); ?>},
                            success: function (data) {
                                var data = JSON.stringify(data);
                                jQuery('.our_pack_size').attr('data-product_variations',data);
                            }
                        });
                    });
                </script>
                <?php                 
        }

        function change_our_pack_size_on_variation_change() {
            $show_settings = get_option('wcim_show_default_pack_setting');
                global $post;
                if (is_single($post->ID)) {
                    $manual_pack_size_setting = get_post_meta($post->ID,'wcim_manual_pack_size_setting',true);
                    if($manual_pack_size_setting == 1 || ($manual_pack_size_setting == 0 && $show_settings == 1)){
                        wp_enqueue_script(array('jquery'));
                        ?>
                    <script>
                        jQuery(document).ready(function () {
                            jQuery('.single_variation_wrap').on('show_variation', function (event, variation) {
                                var variation_id = jQuery('.variation_id').val();
                                var variation_data = jQuery('.our_pack_size').attr('data-product_variations');
                                variation_data = JSON.parse(variation_data);
                                for(var i = 0; i < variation_data.length; i++) {
                                    if (variation_data[i].variation_id == variation_id) {
                                        if (jQuery('.our_pack_size').length) {
                                            jQuery('.our_pack_size').empty();
                                        }
                                        jQuery('.our_pack_size').append(variation_data[i].msg);
                                    }
                                }
                            });
                            jQuery('.variations select').change(function(){
                                    if( jQuery(this).val() == '' ){
                                            jQuery('.our_pack_size').html( jQuery('.our_pack_size').data('default-message') );			
                                    }
                            });
                        });

                    </script>
                    <?php
                }
            }
        }

        function save_inventory_settings() {
            if (isset($_POST['save_inventory_settings'])) {
                $default_our_pack_size = $_POST['default_our_pack_size'];
                $default_supplier_pack_size = $_POST['default_supplier_pack_size'];
                        if ((isset($default_our_pack_size)) && ($default_our_pack_size != "") && (is_numeric($default_our_pack_size)) && ($default_our_pack_size > 0)) {
                            update_option('wcim_default_our_pack_size', $default_our_pack_size);
                            $flag=true;
                        } else {
                            add_action('admin_notices', array($this, 'inventory_settings_error'));
                            return;
                        }
                        if ((isset($default_supplier_pack_size)) && ($default_supplier_pack_size != "") && (is_numeric($default_supplier_pack_size)) && ($default_supplier_pack_size > 0)) {
                            update_option('wcim_default_supplier_pack_size', $default_supplier_pack_size);
                            $flag=true;
                        } else {
                            add_action('admin_notices', array($this, 'inventory_settings_error'));
                            return;
                        }
                        if(isset($_POST['show_pack_settings'])){
                            update_option('wcim_show_default_pack_setting', true);
                            $flag=true;
                        }else{
                            update_option('wcim_show_default_pack_setting',false);
                            $flag=true;
                        }
                        if(isset($_POST['show_one_pack_settings'])){
                            update_option('wcim_show_one_pack_settings', true);
                            $flag=true;
                        }else{
                            update_option('wcim_show_one_pack_settings',false);
                            $flag=true;
                        }
                        if(isset($_POST['default_supplier_currency'])){
                            update_option('wcim_default_supplier_currency',$_POST['default_supplier_currency']);
                            $flag=true;
                        }
                        if(isset($_POST['im_units'])){
                            update_option('wcim_default_unit',$_POST['im_units']);
                            $flag=true;
                        }
                        update_option('wcim_receiver',$_POST['im_receiver'] );
                        update_option('wcim_receiver_address1', $_POST['im_address1']);
                        update_option('wcim_receiver_address2', $_POST['im_address2']);
                        update_option('wcim_receiver_city',$_POST['im_city'] );
                        update_option('wcim_receiver_state',$_POST['im_state'] );
                        update_option('wcim_receiver_zip_code', $_POST['im_zip_code']);
                        update_option('wcim_receiver_contact',$_POST['im_contact'] );
                        if($_POST['im_country']){
                             update_option('wcim_receiver_country', $_POST['im_country']);
                             $flag=true;
                        }
                        if($flag == true){
                            add_action('admin_notices', array($this, 'inventory_settings_success'));
                        }
            }
        }

        function inventory_settings_error() {
           ?>
                    <div class="error"><p><strong><?php _e('Inventory management - Settings','wooic'); ?></strong></p>
                    <p><?php _e('Invalid ! please enter valid data.','wooic'); ?></p>
                    </div>
            <?php
        }
        
        function inventory_settings_success(){
            ?>
            <div class="updated notice"><p><strong><?php _e('Inventory management - Settings','wooic'); ?></strong></p>
                    <p><?php _e('Your settings are successfully saved.','wooic'); ?></p>
                    </div>
                    <?php
        }
        function save_unit_to_meta(){
            $data = array();
            $units = get_option('wcim_units');
            if(!is_array($units)){
                 $units = array();
            }
            $singular_name = $_POST['singular'];
            $plural_name = $_POST['plural'];
            $newArr = array_combine($singular_name, $plural_name);
            $default= array("piece"=>"pieces");
            $units = array_merge($default,$newArr);
            update_option('wcim_units', $units);
            $data['msg'] = __("Added to list",'wooic');
            $data['units'] = $units;
            echo json_encode($data);            
            die;
        }
        function delete_unit_from_meta(){
            $units = get_option('wcim_units');
            $unit_key = $_POST['unit_key'];
            if (array_key_exists($unit_key,$units) && $unit_key != "piece"){
                unset($units[$unit_key]);
                update_option('wcim_units', $units);
                $data['msg'] = "Unit Deleted";
                echo json_encode($data); 
            }else{
                $data['msg'] = "Something went wrong";
                echo json_encode($data);
            }
            die;
        }
        function add_database_updation_notice(){
            if (isset($_GET['page']) && $_GET['page'] == "wc-inventory-management") {
                add_action('admin_notices', array($this, 'display_database_updation_notice'));
            }
        }
        function display_database_updation_notice(){
            $im_default_low_stock_data = get_option('wcim_default_low_stock_data');
            if(!$im_default_low_stock_data){
            ?>
            <div class="notice notice-info">
                <p><strong><?php _e('Inventory management - Update database values !','wooic'); ?></strong></p>
                <p><a href="javascript:void(0);" id="update_database" class="button button-primary"><?php _e('Update data', 'wooic'); ?></a>
                </p>
            </div>
            <?php
            }
        }
        function get_product_data_to_update(){
            $args = array(
                'post_type' => array('product', 'product_variation'),
                'post_status' => array('publish','private'),
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_manage_stock',
                        'value' => 'yes',
                        'compare' => '=',
                    ),
                    array(
                        'key' => 'wcim_supplier_id',
                        'compare' => 'NOT EXISTS',
                    )
                ),
                'posts_per_page' => 1,
            );
            $product_list = new WP_Query($args);
            $arr = array();
            $arr['found_post'] = $product_list->found_posts;    
            echo json_encode($arr);
            die;
        }
        function add_default_data_to_existing_product(){
            $args = array(
                'post_type' => array('product', 'product_variation'),
                'post_status' => array('publish','private'),
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_manage_stock',
                        'value' => 'yes',
                        'compare' => '=',
                    ),
                    array(
                        'key' => 'wcim_supplier_id',
                        'compare' => 'NOT EXISTS',
                    )
                ),
                'posts_per_page' => 10,
            );
            $product_list = new WP_Query($args);
            $arr = array();
            $arr['found_post'] = $product_list->found_posts;    
            if ($product_list->have_posts()) {
                while ($product_list->have_posts()) {
                    $product_list->the_post();
                    $id = get_the_ID();
                    update_post_meta($id,'wcim_supplier_id', 0);
                    update_post_meta($id,'wcim_supplier_show_in_low_stock', "yes");
                    update_post_meta($id,'wcim_supplier_warning_level', 0);
                }
            }
            if($product_list->found_posts == 0){
                update_option('wcim_default_low_stock_data',1);
            }
            echo json_encode($arr);
            die;
        }
        function add_stock_values_menu(){
            $license_verified = get_option('wcim_license_verified');
            if ($license_verified) {
                add_submenu_page('wc-inventory-management', __('Stock values','wooic'), __('Stock values','wooic'), 'view_woocommerce_reports', 'stock-values', array($this, 'display_stock_values'));  
                           
                add_submenu_page('wc-inventory-management', __('Order status','wooic'), __('Order status','wooic'), 'view_woocommerce_reports', 'order-status', array($this, 'order_status_table'));  
            }
        }
        function display_stock_values(){
            wp_enqueue_style('wooicp_style');
            require_once( WOOICP_CLASSES . 'class.data_stock.php' );
            $data = new DataStock();
            $data->prepare_items();
            include( WOOICP_TEMPLATE . 'stock_values.php' );    
        }
        
        function order_status_table(){
			require_once( WOOICP_CLASSES . 'class.order_status_list.php');
			echo '<div class="wrap">';
			echo sprintf('<h2 class="wp-heading-inline">%s</h2>', __('Order status') );
			$obj = new order_status_list();
			$obj->prepare_items();
			$obj->display();
			echo '</div>';
		}
		
		function restore_product_stock_callback( $order_id ){
			
			$stock = get_post_meta( $order_id, 'update_stock', true );
			if( $stock ){
				return;	
			}
			
			$order = wc_get_order( $order_id );
			$items = $order->get_items();	
			foreach ( $items as $item ) {
				$product_id = $item->get_product_id();
				$product_variation_id = $item->get_variation_id();
				if( $product_variation_id ){
					$product_id = $product_variation_id;
				}
				
				$manage_stock = get_post_meta( $product_id, '_manage_stock', true );
				if( $manage_stock == 'no' ){
					$_stock_status = get_post_meta( $product_id, '_stock_status', true );
					if( $_stock_status == 'instock' ){
						return;
					}
				}
				$order_qty = $item->get_quantity();
				$stock = get_post_meta( $product_id, '_stock', true );
				update_post_meta( $product_id, '_stock', $order_qty + $stock );
			}
			update_post_meta( $order_id, 'update_stock', 1 );
		}

		function restore_product_stock_from_cancel_to_processing_callback( $order_id ){
			$stock = get_post_meta( $order_id, 'update_stock', true );
			if( $stock ){
				$order = wc_get_order( $order_id );
				$items = $order->get_items();	
				foreach ( $items as $item ) {
					$product_id = $item->get_product_id();
					$product_variation_id = $item->get_variation_id();
					if( $product_variation_id ){
						$product_id = $product_variation_id;
					}
										
					$manage_stock = get_post_meta( $product_id, '_manage_stock', true );
					if( $manage_stock == 'no' ){
						$_stock_status = get_post_meta( $product_id, '_stock_status', true );
						if( $_stock_status == 'instock' ){
							return;
						}
					}
					$order_qty = $item->get_quantity();
					$stock = get_post_meta( $product_id, '_stock', true );
					update_post_meta( $product_id, '_stock', $stock - $order_qty );
				}
				delete_post_meta( $order_id, 'update_stock', 1 );
			}
		}

		function update_wc_stock_after_edit_qty( $order_id, $items ) {
				
			$order = wc_get_order( $order_id );
			$order_items = $order->get_items();	
			foreach ( $order_items as $o_item_id=>$item ) {
				$product_id = $item->get_product_id();
				$product_variation_id = $item->get_variation_id();
				if( $product_variation_id ){
					$product_id = $product_variation_id;
				}
				$order_qty = $item->get_quantity();
				$updated_qty = $items['order_item_qty'][$o_item_id];
				$diff_qty = abs($order_qty - $updated_qty);
				$stock = get_post_meta( $product_id, '_stock', true );
						
				if( $order_qty > $updated_qty )
				{
					update_post_meta( $product_id, '_stock', $stock + $diff_qty );
				}
				else
				{
					update_post_meta( $product_id, '_stock', $stock - $diff_qty );
				}
				
			}
			
		}

		function update_stock_after_delete_order_item( $item_get_id ) { 
			$variation_id = wc_get_order_item_meta($item_get_id,'_variation_id',true);
			$product_id = wc_get_order_item_meta($item_get_id,'_product_id',true);
			$order_qty = wc_get_order_item_meta($item_get_id,'_qty',true);
			
			if($variation_id) 
			{
				$product_id = $variation_id;
			}
			$stock = get_post_meta($product_id,'_stock',true);	
			
			update_post_meta( $product_id, '_stock', $stock + $order_qty );	
			
		} 

		function update_stock_after_add_order_item( $item_id, $item ) { 
			
			$variation_id = wc_get_order_item_meta($item_id,'_variation_id',true);
			$product_id = wc_get_order_item_meta($item_id,'_product_id',true);
			$order_qty = wc_get_order_item_meta($item_id,'_qty',true);
			
			if($variation_id) 
			{
				$product_id = $variation_id;
			}
			$stock = get_post_meta($product_id,'_stock',true);	
			
			update_post_meta( $product_id, '_stock', $stock - $order_qty );	
		}
    }

    new WooICP_MAIN();
}
