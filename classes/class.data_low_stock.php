<?php
// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class DataLowStock extends WP_List_Table
{
    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        if(isset($_GET['items_per_page'])){
            $perPage = $_GET['items_per_page'];
        }else{
            $perPage = 25;
        }
        global $total_pagination_records;
        $this->set_pagination_args( array(
            'total_items' => $total_pagination_records,
            'per_page'    => $perPage
        ) );

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'thumb' => '<span class="wc-image tips" data-tip="' . esc_attr__('Image', 'woocommerce') . '">' . __('Image', 'woocommerce'),
            'product_sku' =>  __('SKU', 'wooic' ),
            'product_name' => __('Product name', 'wooic' ),
            'product_variant' => __('Product variant', 'wooic' ),
            'product_supplier' => __('Product URL', 'wooic' ),
            'purchase_price' => '<i class="far fa-money-bill-alt" title="'.__('Purchase price', 'wooic' ).'"></i>',
            'warning_level' => '<i class="fas fa-exclamation-triangle" title="'.__('Warning level', 'wooic' ).'"></i>',
            'supplier_pack_size'  => '<i class="fas fa-dolly-flatbed" title="'.__('Supplier pack size', 'wooic' ).'"></i>',
            'our_pack_size'=> '<i class="fas fa-dolly" title="'.__('Our pack size', 'wooic' ).'"></i>',
            'product_stock' => '<i class="fas fa-box" title="'.__('QTY in stock', 'wooic' ).'"></i>',
            'total_pieces'=> '<i class="fas fa-boxes" title="'.__('Units in stock', 'wooic' ).'"></i>',
            'amount' => __('QTY', 'wooic' )
        );
        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('product_name' => array('product_name', false));
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
		global $default_supplier;
                $data = array();
		wp_enqueue_style('woocommerce_admin_styles');
                if(isset($_GET['items_per_page'])){
                    $perPage = $_GET['items_per_page'];
                }else{
                    $perPage = 25;
                }
                global $wpdb;
                $sql = 'SELECT DISTINCT(post_parent) FROM '.$wpdb->posts.' WHERE post_parent > 0';
                $variable_products = $wpdb->get_col( $sql );
                $ordered_product = WooICP_Order::get_ordered_product();
		if( is_array( $ordered_product ) && count( $ordered_product ) ){
                    $post_not_in = array_merge($variable_products,$ordered_product);
                }else{
                    $post_not_in = $variable_products;
                }
                $paged = isset($_REQUEST['paged']) ? $_REQUEST['paged'] : 1 ;
                if(isset($_GET['supplier_id']) &&  $_GET['supplier_id'] !== "all" ){
                    $meta_query = array(
                            'key'	=>	'wcim_supplier_id',
                            'compare'	=>	'=',
                            'value'	=>	$_GET['supplier_id'],
                            'type'	=>	'NUMERIC'
                    );
                }else{
                    $meta_query = array(
                            'key' => 'wcim_supplier_id',
                            'compare' => 'EXISTS',
                    );
                }
		$args = array(
			'post_type' => array('product', 'product_variation'),
			'meta_query' => array(
                            'relation'	=>	'AND',
                            array(
                                'key'	=>	'_stock',
                                'compare'	=>	'<=',
                                'value'	=>	'mt1.meta_value',
                                'type'	=>	'NUMERIC'
                            ),
                            array(
                                'key'	=> 'wcim_supplier_warning_level',
                                'compare'	=>	'EXISTS'
                            ),
                            array(
                                'key' => 'wcim_supplier_show_in_low_stock',
                                'compare' => '=',
                                'value' => "yes"
                            ),
                            'supplier' => $meta_query
                        ),
                        'orderby' => 'supplier',
                        'post__not_in' => $post_not_in,
                        'post_status' => array( 'private', 'publish' ),
			'posts_per_page' => $perPage,
                        'paged'=> $paged,
		);
		
		add_filter( 'posts_request', array( $this, 'closure' ) );
		$product_list = new WP_Query( $args );
		remove_filter( 'posts_request', array( $this, 'closure' ) );
		$default_our_pack_size = get_option('wcim_default_our_pack_size');
                $default_supplier_pack_size = get_option('wcim_default_supplier_pack_size');
		if( $product_list->have_posts() ){
			while( $product_list->have_posts() ){
				global $post;
				$product_list->the_post();
				$id = get_the_ID();
				$product_supplier 	= get_post_meta( $id, 'wcim_supplier_product_url', true );
				$product_stock 		= (int)get_post_meta( $id, '_stock', true );
				$product_sku		= get_post_meta( $id, '_sku', true );
				$warning_level 		= get_post_meta( $id, 'wcim_supplier_warning_level', true );
				$purchase_price 	= get_post_meta( $id, 'wcim_supplier_purchase_price', true );
				$product_supplier_id    = get_post_meta( $id, 'wcim_supplier_id', true );
				$purchase_currency 	= get_post_meta( $product_supplier_id, 'wcim_supplier_currency', true );
				$purchase_price		= $purchase_price ? $purchase_price : 0;
				$product_type		= $post->post_type;
				$product_title		= $product_type == 'product' ? get_the_title() : get_the_title( $post->post_parent );
				$actual_product_id	= $product_type == 'product' ? $id : $post->post_parent;
				$product_variant	= '-';	
                                $supplier_pack_size            = get_post_meta( $id, 'wcim_supplier_pack_size', true );
                                $our_pack_size      = get_post_meta( $id, 'wcim_our_pack_size', true );
                                $our_pack = $our_pack_size ? $our_pack_size : ($default_our_pack_size ? $default_our_pack_size : 1);
                                $supplier_remaining_pieces = (int)get_post_meta($id, 'wcim_supplier_remaining_pieces', true);
                                $total_pieces = floor($product_stock*$our_pack+$supplier_remaining_pieces);
				$image = '<img width="40" height="40" src="'.plugins_url('/woocommerce/assets/images/placeholder.png' ).'" />';
				if( $product_type == 'product_variation' ){
					$product = new WC_Product_Variation( $id );
                                        $product_variant = $product->get_variation_attributes();
                                        if (is_array($product_variant) && count($product_variant)) {
                                          $variation_names = array();
                                          foreach ($product_variant as $key=>$value) {
                                            $term = get_term_by('slug', $value, str_replace("attribute_","", $key) );
                                            if(!$term){
                                                $variation_names[] = $value;
                                            }else{
                                                $variation_names[] = $term->name;
                                            }
                                          }
                                          $product_variant = implode(' | ', $variation_names );
                                        } else {
                                          $product_variant = '-';
                                        }
					if( !$product_sku ){
						$product_sku	= get_post_meta( $post->post_parent, '_sku', true );	
					}
					if( has_post_thumbnail( $id ) ){
						$image = get_the_post_thumbnail($id, array(40,40));	
					}else{
						if( has_post_thumbnail( $post->post_parent ) ){
							$image = get_the_post_thumbnail($post->post_parent, array(40,40));	
						}	
					}
				}else{
					if( has_post_thumbnail( $id ) ){
						$image = get_the_post_thumbnail($id, array(40,40));
					}
                                        
				}
				
				$product_title = sprintf( '<a href="%s" >%s</a>', get_edit_post_link( $actual_product_id ), $product_title);
                                $data[] = array(
                                     'id'				=>  $id,
                                     'thumb'				=>	$image,
                                     'product_name'		=>	$product_title,
                                     'product_sku'		=>	$product_sku,
                                     'product_variant'	=>  $product_variant,
                                     'product_supplier'      =>	$product_supplier,
                                     'product_stock'		=>  $product_stock ? $product_stock : 0,
                                     'warning_level'		=>  $warning_level ? $warning_level : 0,
                                     'purchase_price'	=>  $purchase_price.' '.get_woocommerce_currency_symbol( $purchase_currency ),
                                     'supplier_pack_size'           =>  $supplier_pack_size ? $supplier_pack_size : $default_supplier_pack_size,
                                     'our_pack_size' => $our_pack,
                                     'total_pieces' => $total_pieces
                                 );
			}
		}
		wp_reset_query();
        
        return $data;
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'thumb':
            case 'product_name':
            case 'product_sku':
            case 'product_variant':
            case 'product_stock':
            case 'warning_level':
                case 'supplier_pack_size':
                case 'our_pack_size':
			case 'purchase_price':
                            case 'total_pieces':
				if( isset( $item[ $column_name ] ) && $item[ $column_name ] == '' && $item[ $column_name ] === FALSE ){
					return '-';
				}
                return $item[ $column_name ];
            case 'product_supplier':
				if( !empty($item[ $column_name ]) ){
					return sprintf('<a target="_blank" href="%s">%s</a>', $item[ $column_name ], __( 'URL to product', 'wooic' ) );	
				}
				return '-';
			case 'amount':	
				$supplier = get_post_meta( $item['id'], 'wcim_supplier_id', true );
                                if($supplier && $supplier != 0){
                                    $qty_column = '<input type="text" class="arrived_qty_handler" data-id="'.$item['id'].'" name="product['. $item['id'] .'][qty]" /><input type="hidden" name="product['. $item['id'] .'][stock]" value="'. $item['product_stock'] .'" /><input type="hidden" name="product['. $item['id'] .'][supplier]" value="'. $supplier .'" />';
                                }else{
                                    $qty_column = '<input type="text" class="arrived_qty_handler no_supplier" title="'.__('This product has no supplier assigned','wooic').'" disabled />';
                                }
				return $qty_column;
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'product_name';
        $order = 'asc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }
	
	function closure($sql){
	    return str_replace( "'mt1.meta_value'", "mt1.meta_value", $sql );
	}
}
	
?>