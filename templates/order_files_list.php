	<div class="order_product_files">
		<h2><?php _e('Purchase orders','wooic' );?></h2>
		<table class="widefat striped">
			<thead>
				<tr>
					<th><?php _e('Supplier' ,'wooic');?></th>
					<th><?php _e('Order date','wooic' );?></th>
					<th><?php _e('Products','wooic')?></th>
					<th><?php _e('Purchase price','wooic')?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				if( is_array( $remaning_orders ) && count( $remaning_orders ) ){
					$site_url = site_url('wp-admin');
					foreach( $remaning_orders as $remaning_order ){
						$supplier_name = get_the_title( $remaning_order->supplier_id );
						$link =  add_query_arg(
									array(
										'action'	=>	'download_order_file',
										'supplier'	=>	$remaning_order->supplier_id,
										'date'		=>	strtotime( $remaning_order->order_date )
									),
									$site_url );
						$total_products = WooICP_Order::get_total_products( array( 'supplier_id' => $remaning_order->supplier_id, 'order_date' => $remaning_order->order_date ) );
						$total_purchase = WooICP_Order::get_total_purchase_amount( array( 'supplier_id' => $remaning_order->supplier_id, 'order_date' => $remaning_order->order_date ) );			 
						echo sprintf( '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><a target="_blank" href="%s">%s</a></td></tr>', $supplier_name, $remaning_order->order_date, $total_products, $total_purchase, $link,  __('Download','wooic') );
					}
				}else{
					echo sprintf( '<tr><td colspan="5">%s</td></tr>', __('No current purchase order files.', 'wooic') );
				}
				?>
			</tbody>
		</table>
	</div>