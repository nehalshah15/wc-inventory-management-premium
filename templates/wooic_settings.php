<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e('Inventory management settings', 'wooic') ?></h1>
    <form name="purchase_license_form" action="" method="post">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="Purchase Key"><?php _e('Purchase key', 'wooic') ?></label></th>
                    <td style="width: 350px;"><input type="text"  name="purchase_key" id="purchase_key" placeholder="Enter your purchase key" class="regular-text" value="<?php echo $license_key; ?>">
                    </td>
                    <td  style="width: 15px;">
                        <?php
                        if ($license_verified == true) {
                            ?>
                            <img src="<?php echo WOOICP_PLUGIN_URL . 'images/check-20.png'; ?>" style="margin-top: 1%;"/>
                            <?php
                        } else if ($license_key && !$license_verified) {
                            ?>
                            <img src="<?php echo WOOICP_PLUGIN_URL . 'images/red_cross_tick_20x20.png'; ?>" style="margin-top: 1%;"/>
                        <?php }
                        ?>
                    </td>
                    <td>
                        <input type="submit" name="verify_purchase_key_btn" value="<?php _e('Verify', 'wooic') ?>" class="button button-primary button-large">
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <form name="im_settings_form" action="" method="post">
        <table class="form-table" id="setting_table">
            <tbody>
                <tr>
                    <th><?php _e('Our default pack size', 'wooic') ?></th>
                    <td><input type="text" name="default_our_pack_size" value="<?php echo $default_our_pack_size ? $default_our_pack_size : 1 ?>" >
                        <p class="description" id="tagline-description"><?php _e('This is how many units of this product you sell to your customers when they buy "1". If you sell this product as 5-pack then enter the value 5.', 'wooic') ?></p>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Supplier default pack size', 'wooic') ?></th>
                    <td><input type="text" name="default_supplier_pack_size" value="<?php echo $default_supplier_pack_size ? $default_supplier_pack_size : 1 ?>">
                        <p class="description" id="tagline-description"><?php _e('This is how many units of this product you will get when you buy "1" from your supplier.', 'wooic') ?></p></td>
                </tr>
                <tr>
                    <th><?php _e('Shows the pack size info on product page', 'wooic') ?></th>
                    <td><input name="show_pack_settings" type="checkbox" id="show_pack_settings" <?php echo $show_settings ? "checked" : "" ?>>
                        <p class="description"><?php _e('Shows the pack size information on all product pages.', 'wooic') ?></p>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Hide pack size if pack size is only 1 unit', 'wooic') ?></th>
                    <td><input name="show_one_pack_settings" type="checkbox" id="show_one_pack_settings" <?php echo $show_one_pack_settings ? "checked" : "" ?>>
                        <p class="description"><?php _e('Hides the pack size information on all product pages if it\'s only one unit in the pack size.', 'wooic') ?></p>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Default supplier currency', 'wooic') ?></th>
                    <td class="forminp">
                        <select id="default_supplier_currency" name="default_supplier_currency" data-placeholder="<?php esc_attr_e('Choose a currency&hellip;', 'wooic'); ?>" class="location-input wc-enhanced-select dropdown">
                            <option value=""><?php esc_html_e('Choose a currency&hellip;', 'wooic'); ?></option>
                            <?php
                            asort($currencies);
                            foreach ($currencies as $code => $name) :
                                ?>
                                <option value="<?php echo esc_attr($code); ?>" <?php selected($default_supplier_currency, $code); ?>>
                                    <?php printf(esc_html__('%1$s (%2$s)', 'woocommerce'), $name, get_woocommerce_currency_symbol($code)); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Default unit', 'wooic') ?></th>
                    <td>
                        <select id="im_units" name="im_units">

                            <?php
//                            $units = get_option('wcim_units');
                            if ($units) {
                                foreach ($units as $key => $value) {
                                    ?>
                                    <option value="<?php echo $key; ?>" <?php
                                    if ($key == $default_unit) {
                                        echo "selected";
                                    }
                                    ?>><?php echo __($key, 'wooic') . "/" . __($value, 'wooic'); ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                        </select>
                        <p class="description"><?php _e('This will be the defualt unit for all products.', 'wooic') ?></p>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Units', 'wooic') ?></th>
                    <td><a href="javascript:void(0);" class="button button-primary button-large add_unit_btn"><?php _e('Add unit', 'wooic') ?></a></td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <table id="display_units">
                            <tr>
                                <th><?php _e('Singular', 'wooic') ?></th>
                                <th><?php _e('Plural', 'wooic') ?></th>
                            </tr>
                            <tr>
                                <td class="default_unit">
                                    <input type="text" disabled class="singular_unit" value="<?php _e('piece', 'wooic') ?>">
                                </td>
                                <td>
                                    <input type="text" disabled class="singular_unit" value="<?php _e('pieces', 'wooic') ?>">
                                </td>

                            </tr>
                            <?php
                            if ($units) {
                                array_shift($units);
                                foreach ($units as $key => $value) {
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="text" name="singular_unit[]" class="singular_unit" placeholder="<?php _e('Ex. meter', 'wooic') ?>" value="<?php echo $key; ?>">
                                        </td>
                                        <td>
                                            <input type="text" name="plural_unit[]" class="plural_unit" placeholder="<?php _e('Ex. meters', 'wooic') ?>" value="<?php echo $value; ?>">
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="unit_delete button button-primary button-large"><?php _e('Delete', 'wooic') ?></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </td>
                </tr>
                <tr id="unit_save_tr">
                    <td></td>
                    <td><a href="javascript:void(0);" class="unit_save button button-primary button-large"><?php _e('Save units', 'wooic') ?></a>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Shipping address', 'wooic') ?></th>
                    <td>
                        <table id="company_shipping_address">
                            <tr>
                                <td><?php _e('Receiver', 'wooic') ?></td>
                                <td><input type="text" name="im_receiver" value="<?php echo $im_receiver ? $im_receiver : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('Contact person', 'wooic') ?></td>
                                <td><input type="text" name="im_contact" value="<?php echo $im_contact ? $im_contact : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('Address line 1', 'wooic') ?></td>
                                <td><input type="text" name="im_address1" value="<?php echo $im_address1 ? $im_address1 : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('Address line 2', 'wooic') ?></td>
                                <td><input type="text" name="im_address2" value="<?php echo $im_address2 ? $im_address2 : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('City', 'wooic') ?></td>
                                <td><input type="text" name="im_city" value="<?php echo $im_city ? $im_city : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('State / Province / Region', 'wooic') ?></td>
                                <td><input type="text" name="im_state" value="<?php echo $im_state ? $im_state : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('Zip / Postal code', 'wooic') ?></td>
                                <td><input type="text" name="im_zip_code" value="<?php echo $im_zip_code ? $im_zip_code : ""; ?>"></td>
                            </tr>
                            <tr>
                                <td><?php _e('Country', 'wooic') ?></td>
                                <td>
                                    <select name="im_country" id="im_country">
                                        <option value=""><?php _e('Select country', 'wooic') ?></option>
                                        <?php
                                        foreach ($countries as $key => $country) {
                                            $selected = $im_country == $key ? 'selected' : '';
                                            echo sprintf('<option %s value="%s">%s</option>', $selected, $key, __($country));
                                        }
                                        ?>
                                        </selct>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>
                        <input type="submit" name="save_inventory_settings" value="<?php _e('Save settings', 'wooic') ?>" class="button button-primary button-large">
                    </th>
                </tr>
            </tbody>
        </table>
    </form>
</div>