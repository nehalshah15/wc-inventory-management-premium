<div id="suppliers_variable_data_panel_<?php echo $loop; ?>" class="show_if_variation_manage_supplier">
    <?php
        foreach ($units as $key => $value) {
            $newKeys[$key]=__($key,'wooic');
        }
    ?>
	<div>
		<?php
			woocommerce_wp_select(
				array(
					'id'          => "wcim_supplier_id_{$loop}",
					'name'        => "wcim_supplier_id[{$loop}]",
					'value'		  => $supplier_id,	
					'label'       => __( 'Supplier', 'wooic' ),
					'wrapper_class'	=>	'form-row form-row-first',
					'options'     => WooICP_MAIN::get_supplier_list( true ),
					'desc_tip'    => 'false',
				)	
			);
			woocommerce_wp_text_input( array(
				'id'                => "wcim_supplier_art_id_{$loop}",
				'name'				=> "wcim_supplier_art_id[{$loop}]",
				'value'		  		=> $this->get_data('wcim_supplier_art_id'),	
				'label'             => __( 'Supplier\'s Art ID', 'wooic' ),
				'desc_tip'    		=> 'true',
				'description'       => __('Enter the article id the supplier use for this product.', 'wooic'),
				'wrapper_class'		=>	'form-row form-row-last'
				)
			);
		?>
	</div>
        <div>
            <?php

                woocommerce_wp_checkbox(array(
                'id' => "wcim_supplier_show_in_low_stock_{$loop}",
                'name' => "wcim_supplier_show_in_low_stock[{$loop}]",
                'value' => $supplier_show_in_low_stock,
                'cbvalue' => "yes",
                'label' => __('Enable warning for low stock', 'wooic'),
                'desc_tip' => 'true',
                'wrapper_class' => 'form-row form-row-first',
                'description' => __('Show this product in low stock warning page if the low quantity level has been reached.', 'wooic'),
                'class'=>'supplier_variable_checkbox'
                    )
            );
            woocommerce_wp_text_input(array(
                'id' => "wcim_supplier_warning_level_{$loop}",
                'name' => "wcim_supplier_warning_level[{$loop}]",
                'value' => $this->get_data('wcim_supplier_warning_level'),
                'label' => __('Low stock warning level', 'wooic'),
                'desc_tip' => 'true',
                'data_type' => 'stock',
                'type' => 'number',
                'wrapper_class' => 'form-row form-row-last',
                'description' => __('Enter value for when this product is considered to be low in stock.', 'wooic'),
                    )
            );
            ?>
        </div>
	<div>
		<?php
                if(!$supplier_currency){
                    $supplier_currency = $default_currency;
                }
		$symbol = get_woocommerce_currency_symbol( $supplier_currency );
		if( $symbol ){
			$symbol = " ($symbol)";
		}
		woocommerce_wp_text_input( array(
			'id'                => "wcim_supplier_purchase_price_{$loop}",
			'name'             	=> "wcim_supplier_purchase_price[{$loop}]",
			'value'				=> $this->get_data('wcim_supplier_purchase_price') ? $this->get_data('wcim_supplier_purchase_price') : 0,
			'label'             => __( 'Purchase price', 'wooic' ).$symbol,
			'desc_tip'    		=> 'false',
			'wrapper_class'	=>	'form-row form-row-first',
			'data_type'			=>	'price',
			)
		);
		woocommerce_wp_select(
                    array(
                        'id' => "wcim_supplier_unit_{$loop}",
                        'name' => "wcim_supplier_unit[{$loop}]",
                        'value' => $unit,
                        'label' => __('Product unit', 'wooic'),
                        'wrapper_class' => 'form-row form-row-last',
                        'options' => $newKeys,
                        'desc_tip' => 'true',
                        'description' => __('Unit of your product.', 'wooic'),
                    )
                );
		?>
	</div>
    <div>
		<?php
			woocommerce_wp_text_input( array(
			'id'                => "wcim_supplier_pack_size_{$loop}",
			'name'                => "wcim_supplier_pack_size[{$loop}]",
                        'value' => $supplier_pack,
			'label'             => __( 'Supplier pack size', 'wooic' ),
			'desc_tip'    		=> 'true',
			'data_type'			=>	'stock',
                        'type'              =>'number',
                                'default' => '1',
			'description'       => __('This is how many units of this product you will get when you buy "1" from your supplier.', 'wooic'),
                         'wrapper_class'		=>	'form-row form-row-first')
		);
                woocommerce_wp_text_input(array(
                        'id' => "wcim_our_pack_size_{$loop}",
                        'name' => "wcim_our_pack_size[{$loop}]",
                        'value' => $our_pack,
                        'label' => __('Our pack size', 'wooic'),
                        'desc_tip' => 'true',
                        'data_type' => 'stock',
                        'type' => 'number',
                                'default' => '1',
                        'description' => __('This is how many units of this product you sell to your customers when they buy "1". If you sell this product as 5-pack then enter the value 5.', 'wooic'),
                        'wrapper_class' => 'form-row form-row-last')
                    );
		?>
	</div>
	<div>
		<?php
                woocommerce_wp_text_input(array(
                    'id' => "wcim_supplier_total_pieces_{$loop}",
                    'name' => "wcim_supplier_total_pieces[{$loop}]",
                    'value' => $total_pieces,
                    'label' => __('Units in stock', 'wooic'),
                    'desc_tip' => 'false',
                    'data_type' => 'stock',
                    'type' => 'number',
                    'wrapper_class' => 'form-row form-row-first',
                        )
                );
                woocommerce_wp_text_input( array(
			'id'                => "wcim_supplier_product_url_{$loop}",
			'name'				=> "wcim_supplier_product_url[{$loop}]",	
			'value'		  		=> $this->get_data('wcim_supplier_product_url'),	
			'label'             => __( 'Supplier\'s product URL', 'wooic' ),
			'desc_tip'    		=> 'true',
			'data_type'			=>	'url',
			'type'				=>	'text',
			'description'       => __('If the supplier has a URL for the product, enter it here.', 'wooic'),
			'wrapper_class'		=>	'form-row form-row-last'
			)
		);	
		?>
	</div>
	<div>
		<?php
			woocommerce_wp_text_input( array(
				'id'          => "wcim_supplier_note_{$loop}",
				'name'		  => "wcim_supplier_note[{$loop}]",	
				'value'		  => $this->get_data('wcim_supplier_note'),	
				'label'       => __( 'Product notes', 'wooic' ),
				'desc_tip'    => true,
				'description' => __( 'Here you can enter the special characteristics of the product', 'wooic' ),
				'wrapper_class' => 'form-row form-row-full',
			) );	
		?>
	</div>
</div>
<div class="clear"></div>