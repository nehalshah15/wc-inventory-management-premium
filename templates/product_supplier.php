<div id="suppliers_data_panel" class="panel woocommerce_options_panel">
	<p class="supplier_description description"><?php _e('This information is used for single type product or default values for the variations in variable products.', 'wooic'); ?></p>
	<?php
        if(!$supplier_currency){
            $supplier_currency = $default_currency;
        }
	$symbol = get_woocommerce_currency_symbol( $supplier_currency );
	if( $symbol ){
		$symbol = " ($symbol)";
	}
        foreach ($units as $key => $value) {
            $newKeys[$key]=__($key,'wooic');
        }
		woocommerce_wp_select(
			array(
				'id'          => 'wcim_supplier_id',
				'label'       => __( 'Supplier', 'wooic' ),
				'options'     => WooICP_MAIN::get_supplier_list( true ),
				'desc_tip'    => 'false',
			)	
		);
		woocommerce_wp_text_input( array(
			'id'                => 'wcim_supplier_art_id',
			'label'             => __( 'Supplier\'s Art ID', 'wooic' ),
			'desc_tip'    		=> 'true',
			'description'       => __('Enter the article id the supplier use for this product.', 'wooic'),
			)
		);
		woocommerce_wp_text_input( array(
			'id'                => 'wcim_supplier_product_url',
			'label'             => __( 'Supplier\'s product URL', 'wooic' ),
			'desc_tip'    		=> 'true',
			'data_type'			=>	'url',
			'type'				=>	'text',
			'description'       => __('If the supplier has a URL for the product, enter it here.', 'wooic'),
			)
		);
                woocommerce_wp_checkbox(array(
                    'id'=>'wcim_supplier_show_in_low_stock' ,
                    'label'=>__('Enable warning for low stock','wooic'),
                    'value' => $supplier_show_in_low_stock,
                    'cbvalue' => "yes",
                    'desc_tip' => 'true',
                    'description' => __('Show this product in low stock warning page if the low quantity level has been reached.', 'wooic'),
                 ));
		woocommerce_wp_text_input( array(
			'id'                => 'wcim_supplier_warning_level',
			'label'             => __( 'Low stock warning level', 'wooic' ),
			'desc_tip'    		=> 'true',
			'data_type'			=>	'stock',
			'type'				=>	'number',
			'description'       => __('Enter value for when this product is considered to be low in stock.', 'wooic'),
			)
		);
		woocommerce_wp_text_input( array(
			'id'          => 'wcim_supplier_note',
			'label'       => __( 'Product notes', 'wooic' ),
			'desc_tip'    => true,
			'description' => __( 'Here you can enter the special characteristics of the product', 'wooic' ),
		) );
		woocommerce_wp_text_input( array(
			'id'                => 'wcim_supplier_purchase_price',
			'label'             => __( 'Purchase price', 'wooic' ).$symbol,
                        'value' => $purchase_price ? $purchase_price : 0,
			'desc_tip'    		=> 'false',
			'data_type'			=>	'price',
			)
		);
                woocommerce_wp_text_input(array(
                    'id' => 'wcim_supplier_total_pieces',
                    'label' => __('Units in stock', 'wooic'),
                    'desc_tip' => 'false',
                    'data_type' => 'stock',
                    'type' => 'number',
                    'value'=> $total_pieces,
                        )
                );
                woocommerce_wp_text_input( array(
			'id'                => 'wcim_supplier_pack_size',
			'label'             => __( 'Supplier pack size', 'wooic' ),
			'desc_tip'    		=> 'true',
			'data_type'			=>	'stock',
                        'type'              =>'number',
                        'value'=> $supplier_pack,
			'description'       => __('This is how many units of this product you will get when you buy "1" from your supplier.', 'wooic'),
			)
		);
                woocommerce_wp_text_input(array(
                        'id' => 'wcim_our_pack_size',
                        'label' => __('Our pack size', 'wooic'),
                        'desc_tip' => 'true',
                        'data_type' => 'stock',
                        'type' => 'number',
                        'value'=> $our_pack,
                        'description' => __('This is how many units of this product you sell to your customers when they buy "1". If you sell this product as 5-pack then enter the value 5.', 'wooic'),
                        )
                );
                woocommerce_wp_select(
                        array(
                            'id' => 'wcim_supplier_unit',
                            'label' => __('Product unit', 'wooic'),
                            'options' => $newKeys,
                            'value'=> $unit,
                            'desc_tip' => 'true',
                            'description' => __('Unit of your product.', 'wooic'),
                        )
                );
                woocommerce_wp_select(
                    array(
                        'id' => 'wcim_manual_pack_size_setting',
                        'label' => __('Display pack size on product page', 'wooic'),
                        'options' => array('Preset','Show','Hide'),
                        'desc_tip' => 'false',
                    )
            );
	?>
</div>