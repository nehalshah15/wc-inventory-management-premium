<table class="form-table">
   <tbody>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_short_name"><?php _e('Custom name', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
            <input name="supplier_short_name" id="supplier_short_name" type="text" value="<?php echo $supplier_short_name; ?>" /> 						
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_address"><?php _e('Address', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <textarea name="supplier_address" id="supplier_address"><?php echo $supplier_address; ?></textarea>
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_country"><?php _e('Country', 'wooic')?></label></label>
         </th>
         <td class="forminp">
			<select name="supplier_country" id="supplier_country">
				<option value=""><?php _e('Select country', 'wooic')?></option>
				<?php 
				foreach( $countries as $key => $country ){
					$selected = $supplier_country == $key ? 'selected' : '';
					echo sprintf('<option %s value="%s">%s</option>', $selected, $key, __( $country ) );	
				}
				?>
			</selct>
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_website_url"><?php _e('Homepage URL', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <input name="supplier_website_url" id="supplier_website_url" type="text" value="<?php echo $supplier_website_url; ?>" />
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_order_url"><?php _e('URL for ordering', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <input name="supplier_order_url" id="supplier_order_url" type="text" value="<?php echo $supplier_order_url; ?>" />
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_currency"><?php _e('Currency', 'wooic')?></label></label>
         </th>
         <td class="forminp">
 			<select id="supplier_currency" name="supplier_currency" data-placeholder="<?php esc_attr_e( 'Choose a currency&hellip;', 'woocommerce' ); ?>" class="location-input wc-enhanced-select dropdown">
 				<option value=""><?php esc_html_e( 'Choose a currency&hellip;', 'woocommerce' ); ?></option>
 				<?php 
				$currencies = get_woocommerce_currencies();
				asort($currencies);
				foreach ( $currencies as $code => $name ) :
					 ?>
 					<option value="<?php echo esc_attr( $code ); ?>" <?php selected( $supplier_currency, $code ); ?>>
 						<?php printf( esc_html__( '%1$s (%2$s)', 'woocommerce' ), $name, get_woocommerce_currency_symbol( $code ) ); ?>
 					</option>
 				<?php endforeach; ?>
 			</select>
         </td>
      </tr><tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_description"><?php _e('Description of supplier', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <textarea name="supplier_description" id="supplier_description"><?php echo $supplier_description; ?></textarea>
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_email"><?php _e('General email address', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <input name="supplier_email" id="supplier_email" type="text" value="<?php echo $supplier_email; ?>" />
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_order_email"><?php _e('Email address for ordering', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <input name="supplier_order_email" id="supplier_order_email" type="text" value="<?php echo $supplier_order_email; ?>" />
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_skype_id"><?php _e('Skype name', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <input name="supplier_skype_id" id="supplier_skype_id" type="text" value="<?php echo $supplier_skype_id; ?>" />
         </td>
      </tr>
      <tr valign="top">
         <th scope="row" class="titledesc">
            <label for="supplier_phone_no"><?php _e('Phone number', 'wooic')?></label>
         </th>
         <td class="forminp forminp-text">
			 <input name="supplier_phone_no" id="supplier_phone_no" type="text" value="<?php echo $supplier_phone_no; ?>" />
         </td>
      </tr>
   </tbody>
</table>