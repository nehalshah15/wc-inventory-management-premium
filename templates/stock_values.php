<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e('Inventory management - Stock values', 'wooic') ?></h1>
    <div class="inventory_management_panel blockUI">
        <div class="supplier_filter_panel">
            <?php
                $stock_data = get_option('wcim_stock_data');
                $stock_years = array();
                if ($stock_data && is_array($stock_data)) {
                    $stock_years = array_keys($stock_data);
                }
                ?>
                <form>
                    <input type="hidden" name="page" value="stock-values" />
                    <select name="year_stock">
                        <option value=""><?php echo __('Select year','wooic'); ?></option>
                        <?php
                        if (count($stock_years)) {
                            foreach ($stock_years as $stock_year) {
                                $selected = '';
                                if (isset($_GET['year_stock']) && $_GET['year_stock'] == $stock_year) {
                                    $selected = 'selected';
                                }
                                echo sprintf('<option %s value="%s">%s</option>', $selected, $stock_year, $stock_year);
                            }
                        } else {
                            $stock_year = date('Y');
                            echo sprintf('<option selected value="%s">%s</option>', $stock_year, $stock_year);
                        }
                        ?>
                    </select>
                    <input type="submit" value="<?php _e('Filter','wooic'); ?>" class="button" />
                </form>
                <?php
            ?>
        </div>

        <div class="product_listing_panel">
            <form>
                <?php
                $data->display();
                wp_nonce_field('wooicp_create_product', 'wooicp_product_nonce');
                ?>
                <input type="hidden" name="action" value="create_product_file" />
            </form>
        </div>
    </div>
</div>