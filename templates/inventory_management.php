<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e('Inventory management', 'wooic') ?></h1>
    <div id="myProgress" style="display: none;">
        <div id="myBar">0%</div>
    </div>
    <?php
    $im_default_low_stock_data = get_option('wcim_default_low_stock_data');
    if(!$im_default_low_stock_data){
        return;
    }
    ?>
    <div class="inventory_management_panel blockUI">
        <div class="tab_panel">
            <nav class="nav-tab-wrapper woo-nav-tab-wrapper">
                <a class="nav-tab  <?php echo!isset($_GET['subpage']) ? 'nav-tab-active' : ''; ?>" href="admin.php?page=wc-inventory-management"><?php _e('Products with low stock', 'wooic') ?></a>
                <a class="nav-tab  <?php echo isset($_GET['subpage']) && $_GET['subpage'] == 'delivery_table' ? 'nav-tab-active' : ''; ?>" href="admin.php?page=wc-inventory-management&subpage=delivery_table"><?php _e('Products awaiting delivery', 'wooic') ?></a>
            </nav>
        </div>

        <div class="supplier_filter_panel">
            <?php
            if (isset($supplier_list) && is_array($supplier_list) && count($supplier_list)) {
                ?>
                <form>
                    <input type="hidden" name="page" value="wc-inventory-management" />
                    <?php
                    if (isset($_GET['subpage'])) {
                        echo '<input type="hidden" name="subpage" value="delivery_table" />';
                    }
                    ?>
                    <select name="supplier_id">
                        <?php
                        $hasValue = false;
                        foreach ($supplier_list as $id => $supplier_name) {    
                            if(isset($_GET['supplier_id']) && $_GET['supplier_id'] != 'all'){
                                $selected = ( isset($_GET['supplier_id']) && $_GET['supplier_id'] == $id ? 'selected' : '' );
                            }else{
                                $selected = '';
                            }
                            if ($selected) {
                                $hasValue = true;
                            }
                            $count = WooICP_MAIN::total_low_stock_products($id);
                            $order_count = WooICP_Order::get_ordered_product($id);
                            if (isset($_GET['subpage'])) {
                                $count = count($order_count);
                            }
                            if( $count ){
                                echo sprintf('<option data-name="%s" data-no="%s" %s value="%s">%s (%s)</option>', $supplier_name, $count, $selected, $id, $supplier_name, $count);
                            } 
                        }
                        if (isset($_GET['supplier_id']) && $_GET['supplier_id'] != "all" && !$hasValue) {
                            echo '<option selected value=""></option>';
                        }
                        ?>
                    </select>
                    <input type="submit" class="button" value="<?php _e('Filter','wooic'); ?>" />
                    <div id="per_page_items">
                    <label><?php _e('Items per page','wooic') ?></label>
                    <select name="items_per_page" id="items_per_page">
                        <?php 
                        $per_page_array =  array(25,50,75,100);
                        foreach ($per_page_array as $value) {
                            $selected = ( isset($_GET['items_per_page']) && $_GET['items_per_page'] == $value ? 'selected' : '' );
                            echo sprintf('<option value="%s" %s>%s</option>', $value, $selected, $value);
                        }
                        ?>
                    </select>
                    <input type="submit" class="button" value="<?php _e('Select','wooic'); ?>" />
                    </div>
                </form>
                <?php
            }
            ?>
        </div>

        <div class="product_listing_panel">
            <form>
                <?php
                $data->display();
                wp_nonce_field('wooicp_create_product', 'wooicp_product_nonce');
                ?>
                <input type="hidden" name="action" value="create_product_file" />
            </form>
        </div>

        <?php
        if (!isset($_GET['subpage'])) {
            ?>		
            <div class="footer_panel">
                <input type="button" class="button button-primary" id="btnCreateProductFile" value="<?php _e('Order and create PO','wooic'); ?>"/>
                <div class="product_handler">
                    <form id="frm_product_handler">
                        <input type="hidden" name="action" value="add_product_manually">
                        <?php wp_nonce_field('wooicp_add_product', 'wooicp_nonce'); ?>
                        <input type="text" name="product_sku" placeholder="<?php _e('Product SKU','wooic'); ?>" />
                        <input type="number" name="qty" placeholder="<?php _e('QTY','wooic'); ?>" />
                        <input type="submit" value="<?php _e('Add product manually', 'wooic'); ?>" class="button" />
                    </form>
                </div>
            </div>
            <?php
        }
        echo $remaning_orders;
        ?>    
    </div>
</div>