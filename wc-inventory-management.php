<?php
/*
 * Plugin Name: Woocommerce inventory management-Premium
 * Plugin URI: #
 * Description: Manage inventory stock levels and product purchases for WooCommerce - Premium
 * Version: 0.0.1
 * Author: Kenny Lundbäck
 * Author URI: #
 * * Text Domain: wooic
 * Domain Path: /languages
 */

define('WOOICP_PLUGIN_FILE', __FILE__);
define('WOOICP_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('WOOICP_PLUGIN_URL', plugin_dir_url(__FILE__));
define('WOOICP_TEMPLATE', WOOICP_PLUGIN_DIR . 'templates/');
define('WOOICP_CLASSES', WOOICP_PLUGIN_DIR . 'classes/');
define('WOOICP_INCLUDES', WOOICP_PLUGIN_DIR . 'includes/');

define('WOOICP_NAME', 'Woocommerce inventory management-Premium');

if (!defined('PHP_TAB')) {
    define('PHP_TAB', "\n\t");
}

// Load plugin files
add_action('plugins_loaded', 'wooicp_init');

if (!function_exists('wooicp_init')) {

    function wooicp_init() {
        $locale = is_admin() && function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
        $locale = apply_filters( 'plugin_locale', $locale, 'wooic' );
        
        unload_textdomain( 'wooic' );
        load_textdomain( 'wooic', WOOICP_PLUGIN_DIR . 'languages/' . "wooic-".$locale . '.mo' );
        load_plugin_textdomain( 'wooic', false, WOOICP_PLUGIN_DIR . 'languages' );
        require_once( WOOICP_CLASSES . 'class.wooic.php' );
    }
    
}
?>